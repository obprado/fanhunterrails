class CreateHabilidadesOpcionales < ActiveRecord::Migration
  def up
    create_table :habilidades_opcionales do |t|
      t.string 'especialidad'
      t.string 'nombre'
      t.integer 'nivel'
      t.timestamp

      t.belongs_to :atributo
    end
  end
  def down
    drop_table :habilidades_opcionales
  end
end
