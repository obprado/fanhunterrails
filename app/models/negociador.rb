class Negociador < ArquetipoReal

  def validaciones narizon = nil
    atributos_al_cinco = narizon.atributos.reject { |atributo| atributo.nombre.eql? "carisma"}
    habilidades_al_cinco = narizon.habilidades.reject { |habilidad| habilidad.habilidad_de_catalogo.atributo_de_catalogo.nombre.eql? "carisma"}
    habilidades_opcionales_al_cinco = narizon.habilidades_opcionales.reject { |habilidad_opcional| habilidad_opcional.habilidad_opcional_de_catalogo.atributo_de_catalogo.nombre.eql? "carisma"}

    carisma = narizon.atributos.select { |atributo| atributo.nombre.eql? "carisma"}
    carisma_habilidades = narizon.habilidades.select { |habilidad| habilidad.habilidad_de_catalogo.atributo_de_catalogo.nombre.eql? "carisma"}
    carisma_habilidades_opcionales = narizon.habilidades_opcionales.select { |habilidad_opcional| habilidad_opcional.habilidad_opcional_de_catalogo.atributo_de_catalogo.nombre.eql? "carisma"}


    limite atributos_al_cinco, 5, narizon
    limite habilidades_al_cinco, 5, narizon
    limite habilidades_opcionales_al_cinco, 5, narizon

    limite carisma, 6, narizon
    limite carisma_habilidades, 6, narizon
    limite carisma_habilidades_opcionales, 6, narizon
  end

 def generar_saldos   
   carisma = AtributoDeCatalogo.find_by_nombre "carisma"
   @arquetipo.saldos.push SaldoEspecifico.new :tipo_de_punto => :punto_de_atributo_especifico, :disponible => true, :tipo_especifico => carisma.nombre.to_sym   
   10.times do
     @arquetipo.saldos.push SaldoPuntoLibreDePoderPorAtributo.new :tipo_de_punto => :punto_libre_de_poder_por_atributo, :disponible => true, :tipo_especifico => carisma.nombre.to_sym   
   end
   @arquetipo.generar_saldos_helper
 end    

end
