class CreateCatalogoDePoderes < ActiveRecord::Migration
  def change
    create_table :catalogo_de_poderes do |t|
      t.string :nombre
      t.integer :precio
      
      t.belongs_to :atributo_de_catalogo

      t.timestamps
    end
  end
end
