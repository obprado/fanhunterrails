class SaldoHijo < Saldo
  
  attr_accessor :saldo_padre

  def gastar!
    @saldo_padre.gastar!
    self
  end

end
