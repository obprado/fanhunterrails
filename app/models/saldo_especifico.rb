class SaldoEspecifico < Saldo
  attr_accessor :tipo_especifico

  def puede_pagar? punto
    super and punto.especificidad.eql? tipo_especifico
  end

end
