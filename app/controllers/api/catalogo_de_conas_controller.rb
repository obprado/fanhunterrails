class Api::CatalogoDeConasController < ApplicationController
  
  def index
    @conas = ConaDeCatalogo.all
  end

  def show
    @cona = ConaDeCatalogo.find params[:id]
    respond_with @cona
  end

end
