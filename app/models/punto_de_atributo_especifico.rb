class PuntoDeAtributoEspecifico < PuntoEspecifico

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = atributo.nombre.to_sym
  end

  attr_accessor :atributo

  def descomponerse
    hijos = []
    hijos.push PuntoDeAtributo.new 
    hijos
  end

  def nombre
    atributo.nombre
  end

end
