class Pago
  attr_accessor :gasto, :satisfacciones

  def initialize attributes = {:satisfacciones => []}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def puntos_por_pagar
    resultado = []
    satisfacciones.each do |satisfaccion|
      resultado.concat satisfaccion.puntos_por_pagar
    end
    resultado
  end

end
