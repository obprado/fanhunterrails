class AddTimeStamps < ActiveRecord::Migration

  def change
    add_timestamps :arquetipos
    add_timestamps :atributos
    add_timestamps :catalogo_de_conas
    add_timestamps :catalogo_de_habilidades
    add_timestamps :catalogo_de_habilidades_opcionales
    add_timestamps :catalogo_de_taras
    add_timestamps :conas
    add_timestamps :conas_de_base
    add_timestamps :habilidades
    add_timestamps :habilidades_opcionales    
    add_timestamps :habilidades_opcionales_de_base
    add_timestamps :narizones
    add_timestamps :poderes          
    add_timestamps :taras          
    add_timestamps :taras_de_base          
    add_timestamps :tipos_de_atributo      
  end

end
