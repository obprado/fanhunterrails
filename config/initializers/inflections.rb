# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format
# (all these examples are active by default):
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end
ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'espia', 'espia'
  inflect.irregular 'taxista', 'taxista'
  inflect.irregular 'mentalista', 'mentalista'


  inflect.irregular 'atributo', 'atributos'
  inflect.irregular 'ficha', 'fichas'
  inflect.irregular 'habilidad', 'habilidades'
  inflect.irregular 'poder', 'poderes'
  inflect.irregular 'narizon', 'narizones'
  inflect.irregular 'cona', 'conas'
  inflect.irregular 'tara', 'taras'  
  inflect.irregular 'precio', 'precios'  
  inflect.irregular 'factura', 'facturas'
  inflect.irregular 'arquetipo', 'arquetipos'  
  inflect.irregular 'narizon', 'narizones'
  inflect.irregular 'atributo', 'atributos'
  inflect.irregular 'tipo_de_atributo', 'tipos_de_atributo'
  inflect.irregular 'habilidad_opcional_de_catalogo', 'catalogo_de_habilidades_opcionales' 
  inflect.irregular 'poder_de_catalogo', 'catalogo_de_poderes'  
  inflect.irregular 'habilidad_opcional', 'habilidades_opcionales'
  inflect.irregular 'habilidad_de_catalogo', 'catalogo_de_habilidades'  
  inflect.irregular 'atributo_de_catalogo', 'catalogo_de_atributos'
  
  inflect.irregular 'cona_de_catalogo', 'catalogo_de_conas'
  inflect.irregular 'tara_de_catalogo', 'catalogo_de_taras'


  inflect.irregular 'tara_de_base', 'taras_de_base'
  inflect.irregular 'cona_de_base', 'conas_de_base'
  inflect.irregular 'habilidad_de_base', 'habilidades_de_base'
  inflect.irregular 'habilidad_opcional_de_base', 'habilidades_opcionales_de_base'

  inflect.irregular 'TaraDeBase', 'TarasDeBase'
  inflect.irregular 'ConaDeBase', 'ConasDeBase'
  inflect.irregular 'HabilidadDeBase', 'HabilidadesDeBase'
  inflect.irregular 'HabilidadOpcionalDeBase', 'HabilidadesOpcionalesDeBase'

  inflect.irregular 'ConaDeCatalogo', 'CatalogoDeConas'
  inflect.irregular 'TaraDeCatalogo', 'CatalogoDeTaras'

  inflect.irregular 'TipoDeAtributo', 'TiposDeAtributo'
  inflect.irregular 'HabilidadOpcionalDeCatalogo', 'CatalogoDeHabilidadesOpcionales' 
  inflect.irregular 'PoderDeCatalogo', 'CatalogoDePoderes'  
  inflect.irregular 'HabilidadOpcional', 'HabilidadesOpcionales'
  inflect.irregular 'HabilidadDeCatalogo', 'CatalogoDeHabilidades'  
  inflect.irregular 'AtributoDeCatalogo', 'CatalogoDeAtributos'
end
