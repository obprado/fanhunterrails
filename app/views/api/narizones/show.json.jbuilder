json.(@narizon, :id, :nombre, :jugador, :arquetipo)
@narizon.ordenar
json.atributos @narizon.atributos do |atributo|

  json.(atributo, :id, :nivel)

  json.atributo_de_catalogo do
    json.(atributo.atributo_de_catalogo, :id, :nombre, :orden, :tipo_de_atributo)
    json.habilidad_opcional_de_catalogo atributo.atributo_de_catalogo.habilidad_opcional_de_catalogo, :id, :nombre
  end

  json.habilidades atributo.habilidades do |habilidad|
    json.(habilidad, :id, :nivel)
    json.habilidad_de_catalogo habilidad.habilidad_de_catalogo, :id, :nombre, :orden
    #json.escala habilidad.escala?
  end

  json.habilidades_opcionales atributo.habilidades_opcionales do |habilidad|
    json.(habilidad, :id, :nivel, :especialidad)
    json.habilidad_opcional_de_catalogo habilidad.habilidad_opcional_de_catalogo, :id, :nombre
  end

end

json.poderes @narizon.poderes do |poder|
  json.(poder, :id, :poder_de_catalogo)
end

json.conas @narizon.conas do |cona|
  json.(cona, :id, :cona_de_catalogo)
end

json.taras @narizon.taras do |tara|
  json.(tara, :id, :tara_de_catalogo)
end
