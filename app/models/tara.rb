class Tara < ActiveRecord::Base
  belongs_to :narizon
  belongs_to :tara_de_catalogo

  def saldos

=begin
    padre = SaldoPadre.new
    padre.hijos.push SaldoHijo.new :tipo_de_punto => tara_de_catalogo.nombre, :disponible => true, :saldo_padre => padre
    tara_de_catalogo.valor.times do
      padre.hijos.push SaldoHijo.new :tipo_de_punto => :punto_libre, :disponible => true, :saldo_padre => padre
    end
    padre.hijos
=end
    saldo = SaldoPuntoDeTara.new :tipo_de_punto => :punto_de_tara_especifica, :disponible => true, :tipo_especifico => tara_de_catalogo.nombre.to_sym, :tara => self
    [saldo]
  end

  def valor
    tara_de_catalogo.valor
  end

  def nombre
    tara_de_catalogo.nombre
  end

end
