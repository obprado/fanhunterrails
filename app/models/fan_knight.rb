# -*- coding: utf-8 -*-
class FanKnight < ArquetipoReal

  def validaciones narizon = nil
    super
    validar_poderes narizon
  end

  def validar_poderes fan_knight
    atributos_permitidos_poderes_fan_knight = ["neuronas","carisma","empatia"]
    fan_knight.poderes.each do |poder|
      unless atributos_permitidos_poderes_fan_knight.include? poder.poder_de_catalogo.atributo_de_catalogo.nombre
        fan_knight.errors.add :flipao, "#{poder.poder_de_catalogo.nombre.titleize} no pertenece a los atributos permitidos para poderes del Fan Knight. Eligete uno de neuronas, carisma o empatia"
      end
    end
  end

end
