class ChangeConasTarasYPoderesBelongToNarizon < ActiveRecord::Migration
  def up
    change_table :conas do |t|
      t.belongs_to :narizon
    end

    change_table :taras do |t|
      t.belongs_to :narizon
    end

    change_table :poderes do |t|
      t.belongs_to :narizon
    end

    remove_column :poderes, :atributo_id
    remove_column :taras, :atributo_id
    remove_column :conas, :atributo_id

  end

  def down
    remove_column :poderes, :narizon_id
    remove_column :taras, :narizon_id
    remove_column :conas, :narizon_id
  end
end
