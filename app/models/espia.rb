class Espia < ArquetipoReal

  def validaciones narizon
    no_tiene_poderes narizon
    super
  end

  def no_tiene_poderes narizon
    if not narizon.poderes.empty?
      narizon.errors.add :poderes, "los espias no tienen poderes"
    end
  end

end
