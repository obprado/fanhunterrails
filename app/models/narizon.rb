class Narizon < ActiveRecord::Base

#  include ActiveModel::ForbiddenAttributesProtection

  def after_initalize
    arquetipo = arquetipo.generar_arquetipo
  end

  belongs_to :arquetipo

  has_many :poderes, :inverse_of => :narizon
  accepts_nested_attributes_for :poderes, :reject_if => lambda { |poder| poder[:poder_de_catalogo_id].blank? }

  has_many :conas
  accepts_nested_attributes_for :conas, :reject_if => lambda { |cona| cona[:cona_de_catalogo_id].blank? }

  has_many :taras
  accepts_nested_attributes_for :taras, :reject_if => lambda { |tara| tara[:tara_de_catalogo_id].blank? }

  has_many :atributos
  accepts_nested_attributes_for :atributos

  has_many :habilidades, :through => :atributos
  has_many :habilidades_opcionales, :through => :atributos

  def habilidades
    habilidades = []
    atributos.each do |atributo|
      habilidades.concat atributo.habilidades
    end
    habilidades
  end

  def habilidades_opcionales
    habilidades_opcionales = []
    atributos.each do |atributo|
      habilidades_opcionales.concat atributo.habilidades_opcionales
    end
    habilidades_opcionales
  end


#  validate :sobran_puntos?, :faltan_puntos?, :validaciones

  def validaciones
    unless arquetipo.nombre.eql? "cadaver"
      arquetipo.validaciones self
    end
  end

  def tipos_de_atributo
    TipoDeAtributo.all
  end

  def pagar
    arquetipo.aplicar_modificadores
    @monedero = Monedero.new :saldos => self.generar_monedero
    @factura = generar_factura
#    @ticket = @factura.cobrar @monedero    
    cobrador = Cobrador.new
    cobrador.realizar_cobros @monedero, @factura 
  end

  def generar_monedero
    saldos = []
    saldos.concat arquetipo.generar_saldos

    taras.each do |tara|
      saldos.concat tara.saldos
    end

    atributos.each do |atributo|
      saldos.concat atributo.saldos
    end

    saldos
  end

  def refill
    poderes.size.upto 8 do
      poderes.build
   end

    conas.size.upto 8 do
      conas.build
    end
    taras.size.upto 8 do
      taras.build
    end
  end

  def sobran_puntos?
    unless arquetipo.nombre.eql? "cadaver"      
      saldos_sobrantes = @monedero.saldos_disponibles #.select { |saldo| saldo.disponible }
      
      unless saldos_sobrantes.empty?
        saldos_normales = saldos_sobrantes.reject { |saldo| saldo.respond_to? :tipo_especifico }
        tipos_de_saldos = saldos_normales.map { |saldo| saldo.tipo_de_punto }
        tipos_de_saldos.uniq!
        tipos_de_saldos.each do |tipo_de_saldo|
          sobrantes = saldos_normales.count { |saldo| saldo.tipo_de_punto.eql? tipo_de_saldo }          
          errors.add :te, "tienes que gastar #{sobrantes} #{tipo_de_saldo.to_s.humanize}"
        end           
      end


      unless saldos_sobrantes.empty?
        saldos_especificos = saldos_sobrantes.select { |saldo| saldo.respond_to? :tipo_especifico }
        tipos_de_saldos = saldos_especificos.map { |saldo| saldo.tipo_especifico }
        tipos_de_saldos.uniq!
        tipos_de_saldos.each do |tipo_de_saldo|
          sobrantes = saldos_especificos.count { |saldo| saldo.tipo_especifico.eql? tipo_de_saldo }          
          errors.add :te, "tienes que gastar #{sobrantes} #{tipo_de_saldo.to_s.humanize}"
        end           
      end
      

    end
  end
  
  def faltan_puntos?
    unless arquetipo.nombre.eql? "cadaver"
      unless @factura.puntos_por_pagar.empty?
        puntos_por_pagar = @factura.puntos_por_pagar
        puntos_libres_por_pagar = puntos_por_pagar.select { |punto| punto.tipo_de_punto.eql? :punto_libre }
        otros_puntos_por_pagar = puntos_por_pagar.reject { |punto| punto.tipo_de_punto.eql? :punto_libre }
        
        unless puntos_libres_por_pagar.empty?
          errors.add :puntos, "necesitas conseguir #{puntos_libres_por_pagar.size} puntos libres"
        end

        unless otros_puntos_por_pagar.empty?
          otros_puntos_por_pagar.each do |punto_por_pagar|
            errors.add :puntos, "te falta un #{punto_por_pagar.to_s.titleize}"
          end
        end
      end
    end
  end

  def generar_factura
    factura = Factura.new
    puntos = []

    puntos.concat arquetipo.gastos

    poderes.each do |poder|
      puntos.concat poder.gasto.puntos
    end
    
    atributos.each do |atributo|
      puntos.concat atributo.gasto.puntos
    end
    
    habilidades.each do |habilidad|
      puntos.concat habilidad.gasto.puntos
    end

    habilidades_opcionales.each do |habilidad_opcional|
      puntos.concat habilidad_opcional.gasto.puntos
    end

    conas.each do |cona|
      puntos.concat cona.gasto.puntos
    end
    
    factura.puntos = puntos
    factura
  end
  
  def ordenar
    atributos.sort_by! { |atributo| atributo.orden }
  end



  def fill_defaults
    updated_at = Time.now
    self.nombre = ""
    self.jugador = ""
    self.arquetipo = Arquetipo.find_by_nombre 'rebelde'
    AtributoDeCatalogo.all.each do |atributo_catalogo|
      atributo = Atributo.new
      atributo.atributo_de_catalogo = atributo_catalogo
      atributo.nivel = 1
      atributos.push atributo
      atributo.narizon = self
      atributo.fill_defaults      
    end

    atributos.sort_by! { |atributo| atributo.orden }
    self.poderes = []
    self.taras = []
    self.conas = []
  end

  def facturables
    facturables = []
    poderes.each do |p|
      facturables.push p
    end
    conas.each do |c|
      facturables.push c
    end
    atributos.each do |a|
      facturables.push a
    end
    habilidades.each do |h|
      facturables.push h
    end
    habilidades_opcionales.each do |h|
      facturables.push h
    end
    facturables
  end

=begin
  def factura
    factura = Factura.new
    facturables.each do |f|
      factura.gastos.push f.gasto
    end
    factura
  end
=end

  def monedero
    @monedero
  end

  def factura
    @factura
  end

  def self.existentes array_hash, symbol
    resultado = []
    array_hash.each do |elto|
      if elto[symbol]
        resultado.push elto
      end
    end
    resultado
  end

  def self.inexistentes array_hash, symbol
    resultado = []
    array_hash.each do |elto|
      unless elto[symbol]
        resultado.push elto
      end
    end
    resultado
  end

  def self.validos array_hash, symbol1, symbol2
    resultado = []
    array_hash.each do |elto|
      if elto[symbol1] and elto[symbol1][symbol2] and !(elto[symbol1][symbol2].eql? 0)
        resultado.push elto
      end
    end
    resultado
  end

  def self.hash_to_narizon hash, narizon
    narizon.nombre = hash[:nombre]
    if narizon.nombre == ""
      narizon.nombre = "Borrador"
    end
    narizon.jugador = hash[:jugador]
#    narizon.arquetipo_id = hash[:arquetipo][:id]
    if  hash[:arquetipo][:id]
      narizon.arquetipo = Arquetipo.find hash[:arquetipo][:id]
    end

    hash[:atributos].each do |atributo|
      atributo_actual = narizon.atributos.select { |att| att.atributo_de_catalogo.id.eql? atributo[:atributo_de_catalogo][:id] }[0]
      atributo_actual.nivel = atributo[:nivel]

      atributo[:habilidades].each do |habilidad|
        habilidad_actual = narizon.habilidades.select { |hab| hab.habilidad_de_catalogo.id.eql? habilidad[:habilidad_de_catalogo][:id] }[0]
        habilidad_actual.nivel = habilidad[:nivel]
      end

      #atributo_actual.habilidades_opcionales.each { |hab| hab.destroy }
      unless atributo[:habilidades_opcionales]
        atributo[:habilidades_opcionales] = []
      end
      
      habilidades_opcionales_existentes = Narizon.existentes atributo[:habilidades_opcionales], :id
      habilidades_opcionales_inexistentes = Narizon.inexistentes atributo[:habilidades_opcionales], :id


      habilidades_opcionales_existentes.each do |habilidad_opcional|        
#        ho = HabilidadOpcional.find habilidad_opcional[:id]
        if habilidad_opcional[:especialidad]
          ho = narizon.habilidades_opcionales.select { |hab| hab.id.eql? habilidad_opcional[:id] }[0]
          unless ho
            ho = HabilidadOpcional.new
            ho.habilidad_opcional_de_catalogo = HabilidadOpcionalDeCatalogo.find habilidad_opcional[:habilidad_opcional_de_catalogo][:id]
          end
          ho.especialidad = habilidad_opcional[:especialidad]
          ho.nivel = habilidad_opcional[:nivel]
          atributo_actual.habilidades_opcionales.push ho
        end
      end

      habilidades_opcionales_inexistentes.each do |habilidad_opcional|        
        habilidad_opcional_actual = HabilidadOpcional.new 
        habilidad_opcional_actual.habilidad_opcional_de_catalogo = HabilidadOpcionalDeCatalogo.find  habilidad_opcional[:habilidad_opcional_de_catalogo][:id]
        habilidad_opcional_actual.nivel = habilidad_opcional[:nivel]
        habilidad_opcional_actual.especialidad = habilidad_opcional[:especialidad]
        atributo_actual.habilidades_opcionales.push habilidad_opcional_actual
      end

    end

    if hash[:poderes]
#      narizon.poderes.each { |pod| pod.destroy }
#      poderes_existentes = existentes hash[:poderes], :id
      poderes_validos = Narizon.validos hash[:poderes], :poder_de_catalogo, :id
#      poderes_validos = Narizon.existentes hash[:poderes], :poder_de_catalogo
      poderes_inexistentes = Narizon.inexistentes poderes_validos, :id

      #el siguiente bloque deberia de poder borrarse
      poderes_inexistentes.each do |poder|
        #p = Poder.find_or_initialize_by :id => poder[:id]
        p = Poder.new
        p.poder_de_catalogo = PoderDeCatalogo.find poder[:poder_de_catalogo][:id]
        narizon.poderes.push p
      end
      
      poderes_validos.each do |poder|
#        unless narizon.poderes.include? poder
          pod = Poder.find(poder[:id])
          pod.poder_de_catalogo = PoderDeCatalogo.find(poder[:poder_de_catalogo][:id])
          narizon.poderes.push pod
 #       end
      end
    end

    if hash[:taras]
      taras_validas = Narizon.validos hash[:taras], :tara_de_catalogo, :id
#      taras_validas = Narizon.existentes hash[:taras], :taras_de_catalogo
      taras_inexistentes = Narizon.inexistentes taras_validas, :id
#      narizon.taras.each { |tar| tar.destroy }
      #este bloque no deberia de hacer falta
      taras_inexistentes.each do |tara|
        #t = Tara.find_or_initialize_by :id => tara[:tara_de_catalogo][:id]
        t = Tara.new
        t.tara_de_catalogo = TaraDeCatalogo.find tara[:tara_de_catalogo][:id]
        narizon.taras.push t
      end

      taras_validas.each do |tara|
        tar = Tara.find(tara[:id])
        tar.tara_de_catalogo = TaraDeCatalogo.find(tara[:tara_de_catalogo][:id])
        narizon.taras.push tar
      end
    end

    if hash[:conas]
      conas_validas = Narizon.validos hash[:conas], :cona_de_catalogo, :id
#      conas_validas = Narizon.existentes hash[:conas], :conas_de_catalogo
      conas_inexistentes = Narizon.inexistentes conas_validas, :id
      # narizon.conas.each { |con| con.destroy }

      #este bloque no deberia de hacer falta
      conas_inexistentes.each do |cona|
        #c = Cona.find_or_initialize_by :id => cona[:cona_de_catalogo][:id]
        c = Cona.new
        c.cona_de_catalogo = ConaDeCatalogo.find cona[:cona_de_catalogo][:id]
        narizon.conas.push c
      end

      conas_validas.each do |cona|
        con = Cona.find(cona[:id])
        con.cona_de_catalogo = ConaDeCatalogo.find(cona[:cona_de_catalogo][:id])
        narizon.conas.push con
      end
    end

    narizon
  end


end
