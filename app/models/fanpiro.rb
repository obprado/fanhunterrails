class Fanpiro < ArquetipoReal
  
  def generar_saldos
    3.times do
      saldo = SaldoPuntoDeFanpiro.new :tipo_de_punto => :punto_de_fanpiro, :disponible => true
      @arquetipo.saldos.push saldo
    end    
    
    @arquetipo.generar_saldos_helper
    
  end    
  
end
