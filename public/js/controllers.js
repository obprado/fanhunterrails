var fhControllers = angular.module('fhControllers', ['ngRoute','config', 'fhDirectives', 'fhEntities']);

fhControllers.controller('portadaCtrl', function($scope, $http, HOST, Narizon, Arquetipo){
});

fhControllers.controller('navbarCtrl', function($scope, $routeParams, $location){
});

fhControllers.controller('narizonesCtrl', function($scope, $http, HOST, Narizon, Arquetipo){
    $scope.narizones = Narizon.query();
});

fhControllers.controller('narizonShowCtrl', function($scope,$http, HOST, $routeParams, $location, Narizon){
    $scope.read_only = true;
    $location.path('narizones/' + $routeParams.id + '/datos');    
});

fhControllers.controller('pestanaCtrl', function($scope,$http, HOST, $routeParams, $location, Narizon, Arquetipo, PoderDeCatalogo, ConaDeCatalogo, TaraDeCatalogo, Atributo, Poder, Categoria, Coleccion, $modal){
    
    $scope.find_arquetipo = function(arquetipos, narizon){
//	alert(34);
//	alert(narizon.arquetipo.id);
//	alert(arquetipos[0].id);
	res = "";
	arquetipos.forEach(function(arquetipo){
	    if (arquetipo.id == narizon.arquetipo.id){
		res = arquetipo;
	    }
	});
	return res;
    };
    
    $scope.pestana = $routeParams.pestana;    

    PoderDeCatalogo.query(function(poderes){ $scope.poderes = poderes});
    ConaDeCatalogo.query(function(conas){ $scope.conas = conas });
    TaraDeCatalogo.query(function(taras){ $scope.taras = taras });
    $scope.isActive = function(elemento){ return $location.url().match(elemento);    };

    Narizon.get($routeParams.id, function(resultado){ 

	$scope.narizon = resultado;    

	$scope.narizon.validar(function(saldos){ 
	    $scope.puntos_de_atributo = saldos.punto_de_atributo ? saldos.punto_de_atributo : 0;
	    $scope.puntos_de_habilidad = saldos.punto_de_habilidad ? saldos.punto_de_habilidad : 0;
	    $scope.puntos_libres = saldos.punto_libre ? saldos.punto_libre : 0;
	}, function(impagados){	    
	    $scope.puntos_de_deuda = impagados.punto_libre;	    
	});

	Arquetipo.query(function(arquetipos){ 
	    $scope.arquetipos = arquetipos; 
	    $scope.find_arquetipo(arquetipos, resultado).active = true;
	});

    });

    $scope.categorias_de_taras = Coleccion(TaraDeCatalogo.categorias(), Categoria);
    $scope.categorias_de_conas = Coleccion(ConaDeCatalogo.categorias(), Categoria);


    $scope.deletePoder = function(narizon, poder){
	narizon.poderes.splice(narizon.poderes.lastIndexOf(poder), 1);
	poder.remove({id: poder.id});
    };

    $scope.deleteTara = function(narizon, tara){
	narizon.taras.splice(narizon.taras.lastIndexOf(tara), 1);
	tara.remove({id: tara.id});
    };

    $scope.deleteCona = function(narizon, cona){
	narizon.conas.splice(narizon.conas.lastIndexOf(cona), 1);
	cona.remove({id: cona.id});
    };    



    $scope.$on("$destroy", function(){
 	Narizon.update({id: $scope.narizon.id}, $scope.narizon);
    });

    

    $scope.open = function () {
	var modalInstance = $modal.open({
	    templateUrl: 'myModalContent.html',
	    controller: narizonBorrarCtrl,
	    size: 'sm',
	    resolve: {
		narizon: function(){return $scope.narizon},
		$location: function(){return $location}
	    }
	});
    };

});

var narizonBorrarCtrl = function($scope, narizon, $location, $modalInstance){

    $scope.borrar = function(){
	narizon.remove({id: narizon.id});
	$location.path('narizones');    	
	$modalInstance.dismiss('cancel');
    };

    $scope.cancel = function(){
	$modalInstance.dismiss('cancel');
    };
};

fhControllers.controller('narizonEditCtrl', function($scope,$http, HOST, $routeParams, $location, Narizon){
    $scope.read_only = false;	    
    load_narizon_info($scope, $http, HOST, $routeParams.id);
    $scope.guardar = function(narizon){
	Narizon.update({narizon: narizon, id: $routeParams.id});
	$location.path(HOST + '/narizones');
    };
});

fhControllers.controller('narizonNewCtrl', function($scope,$http, HOST, Narizon, $location){
    $scope.read_only = false;
//    load_narizon_info($scope, $http, HOST, "new");
    Narizon.create(function(respuesta){	
	$location.path('narizones/' + respuesta.id + '/datos');    
    });
    $scope.guardar = function(narizon){
	Narizon.save({narizon: narizon});
	$location.path(HOST + '/narizones');
    };
//    load_common_info($scope, $http, HOST);
//    $scope.narizon =  nuevo_narizon();
  //  $scope.narizon.atributos_tipo = att_tipo;
});


var att_tipo = function(tipo){
    result = [];
    this.atributos.forEach(function(atributo){
	if (atributo.atributo_de_catalogo.tipo_de_atributo.tipo == tipo){
	    result.push(atributo);	    
	}	
    });
    result.sort(function(a,b){return a.atributo_de_catalogo.orden - b.atributo_de_catalogo.orden});
    return result;    
};

var load_common_info = function($scope, $http, HOST){
    $http.defaults.headers.common.Accept = "application/json";
    $http.get(HOST + '/api/arquetipos').success(function(arquetipos){
	$scope.arquetipos = arquetipos;
    });
    $http.get(HOST + '/api/catalogo_de_poderes').success(function(poderes){
	$scope.poderes = poderes;
	poderes.forEach(function(poder){
	    poder.toString = function(){
		return this.nombre + " (" + this.precio + ")";
	    };
	});
    });
    $http.get(HOST + '/api/catalogo_de_conas').success(function(conas){

	$scope.conas = conas;
	conas.forEach(function(cona){
	    cona.toString = function(){
		return this.nombre + " (" + this.precio + ")";
	    };
	});
    });
    $http.get(HOST + '/api/catalogo_de_taras').success(function(taras){
	$scope.taras = taras;
	taras.forEach(function(tara){
	    tara.toString = function(){
		return this.nombre + " (" + this.valor + ")";
	    };
	});
    });    
};


var load_narizon_info = function($scope, $http, HOST, narizon_id){
    $http.defaults.headers.common.Accept = "application/json";
    $http.get(HOST + '/api/narizones/' + narizon_id)
	.success(function(narizon){
	    narizon.atributos_tipo = att_tipo;
	    $scope.narizon = narizon;
/*	    for (var i = 0; i<2; i++){
		narizon.poderes[i] = new Object();
		narizon.conas[i] = new Object();//new Cona();
		narizon.taras[i] = new Object();//new Tara();
	    }*/
	});
    load_common_info($scope, $http, HOST);
};