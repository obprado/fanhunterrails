class LinkAtributoWithCatalogoDeAtributos < ActiveRecord::Migration
  
  def change
    remove_column :atributos, :nombre
    change_table :atributos do |tabla|
      tabla.belongs_to :atributo_de_catalogo
    end
  end
      
end
