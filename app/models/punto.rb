class Punto

  attr_accessor :pagado


  def initialize attributes = {:pagado => false}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end    
  end

=begin
  def satisfacer_con monedero
    if monedero.puede_pagar self.class.name.classify
      monedero.pagar self.class.name.classify
    else
      @hijos = descomponer
      @hijos.each do |hijo|
        hijo.satisfacer_con monedero
      end
    end
  end
=end

  def descomponer
    @hijos = descomponerse
  end

  def descomponer! coleccion_de_puntos
    coleccion_de_puntos.concat self.descomponer
    coleccion_de_puntos.delete self    
  end

=begin
  def puntos_por_pagar
    por_pagar = []   
    if puede_descomponerse? and @hijos
      @hijos.each do |hijo|
        por_pagar.concat hijo.puntos_por_pagar
      end
    else
      unless pagado?
        por_pagar.push self
      end
    end    
    por_pagar
  end    
=end

  def pagar!
    self.pagado = true
#    coleccion_de_puntos.delete self
  end

  def pagado?
    if puede_descomponerse? and @hijos
      @hijos.select { |hijo| not hijo.pagado? }.empty?
    else
      pagado
    end
  end

  def tipo_de_punto
    self.class.name.underscore.to_sym
  end

  def puede_descomponerse?
    true
  end

end
