class Poder < ActiveRecord::Base
  belongs_to :narizon, :inverse_of => :poderes
  belongs_to :poder_de_catalogo
 
 # validates :narizon, :presence => true
  
#  after_validation :poner_nivel

  def poner_nivel
    @nivel = narizon.atributos.select { |atributo| atributo.atributo_de_catalogo.id.eql? poder_de_catalogo.atributo_de_catalogo.id }.first.nivel
  end

  def coste
    poder_de_catalogo.precio
  end

  def nombre
    poder_de_catalogo.nombre
  end

  def gasto
    poder_de_catalogo.gasto
  end

end
