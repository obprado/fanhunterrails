class Mentalista < ArquetipoReal

  def validaciones narizon = nil
    super
    validar_puntos_de_truco narizon
  end

  def validar_puntos_de_truco mentalista
    puntos_truco = 0
    mentalista.poderes.each { |poder| puntos_truco += poder.coste }
    if puntos_truco > 20
      mentalista.errors.add :los, "puntos de truco no deberian de sumar mas de 20 puntos"
    end
  end

  def generar_saldos   
    20.times do
      @arquetipo.saldos.push SaldoDeTruco.new :tipo_de_punto => :punto_de_truco, :disponible => true
    end
    @arquetipo.generar_saldos_helper
  end    

end
