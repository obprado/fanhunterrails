class AnadirConasTarasAlArquetipo < ActiveRecord::Migration
  def change

    create_table :taras_de_base do |t|
      t.belongs_to :arquetipo
      t.belongs_to :tara_de_catalogo
    end

    create_table :conas_de_base do |t|
      t.belongs_to :arquetipo
      t.belongs_to :cona_de_catalogo
    end

  end
end
