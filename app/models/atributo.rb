class Atributo < ActiveRecord::Base
  belongs_to :narizon
  belongs_to :atributo_de_catalogo

  has_many :habilidades, :inverse_of => :atributo
  accepts_nested_attributes_for :habilidades

  has_many :habilidades_opcionales
  accepts_nested_attributes_for :habilidades_opcionales
  
  def tipo
    atributo_de_catalogo.tipo
  end

  def nombre
    atributo_de_catalogo.nombre
  end

  def orden
    atributo_de_catalogo.orden
  end

  after_initialize :fill_default

  def fill_default
    unless self.nivel 
      self.nivel = 1
    end
  end


  def fill_defaults
    atributo_de_catalogo.catalogo_de_habilidades.each do |habilidad_catalogo|         
      habilidad = Habilidad.new
      habilidad.habilidad_de_catalogo = habilidad_catalogo
      habilidad.nivel = 0
      habilidad.atributo = self
      habilidades.push habilidad
    end

    habilidades.sort_by! { |habilidad| habilidad.habilidad_de_catalogo.orden }

    habilidad_opcional_catalogo = atributo_de_catalogo.habilidad_opcional_de_catalogo
    habilidades_opcionales = []
=begin
    3.times do            
      habilidad_opcional = HabilidadOpcional.new
      habilidad_opcional.atributo = self
      habilidad_opcional.nivel = 0
      habilidad_opcional.habilidad_opcional_de_catalogo = habilidad_opcional_catalogo
      habilidades_opcionales.push habilidad_opcional
    end
=end
    self    
  end

  def gasto
    puntos = []
    nivel.times do
      puntos.push PuntoDeAtributoEspecifico.new :atributo => self, :pagado => false
    end
    Gasto.new :puntos => puntos
  end

  def saldos
    habilidad_gratuita_de_catalogo = atributo_de_catalogo.habilidad_gratuita
    saldos = []
    (nivel/2).times do
      saldos.push SaldoEspecifico.new :tipo_de_punto => :punto_de_habilidad_especifica, :disponible => true, :tipo_especifico => habilidad_gratuita_de_catalogo.nombre.to_sym
    end
    saldos
  end
  
end
