class Api::CatalogoDeTarasController < ApplicationController

  def index
    @taras = TaraDeCatalogo.all
  end

  def show
    @tara = TaraDeCatalogo.find params[:id]
    respond_with @tara
  end

end
