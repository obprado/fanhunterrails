class Metropolis < ActiveRecord::Migration
  def up

    create_table :narizones do |t|
      t.string 'nombre'
      t.string 'arquetipo'

      t.timestamp
    end
    
    create_table :tipos_de_atributo do |t|
      t.belongs_to :narizon
      t.string 'tipo'

      t.timestamp
    end

    create_table :atributos do |t|
      t.belongs_to :tipo_de_atributo
      t.string 'nombre'
      t.integer 'nivel'

      t.timestamp
    end

    create_table :habilidades do |t|
      t.belongs_to :atributo
      t.string 'nombre'
      t.integer 'nivel'
    end

    create_table :habilidades_opcionales do |t|
      t.belongs_to :atributo
      t.string 'nombre'
      t.string 'especialidad'
      t.integer 'nivel'
    end

    create_table :poderes do |t|
      t.belongs_to :atributo
      t.string 'nombre'
      t.integer 'precio'
      t.integer 'nivel'
    end

    create_table :taras do |t|
      t.belongs_to :atributo
      t.string 'nombre'
      t.integer 'precio'
    end

    create_table :conas do |t|
      t.belongs_to :atributo
      t.string 'nombre'
      t.integer 'precio'
    end
  end

  def down
    drop_table :narizones
    drop_table :tipos_de_atributo
    drop_table :atributos
    drop_table :habilidades
    drop_table :habilidades_opcionales
    drop_table :poderes
    drop_table :taras
    drop_table :conas
  end
end
