class AddJugadorToNarizones < ActiveRecord::Migration
  def change
    change_table :narizones do |t|
      t.string :jugador
    end
  end
end
