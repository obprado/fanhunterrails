class SaldoPuntoLibreDePoderPorAtributo < SaldoEspecifico

  def descomponer! coleccion_de_puntos
    coleccion_de_puntos.delete self
  end

  def to_s
    "Punto de poder del atributo #{especificidad}"
  end

end
