class Cityender < ActiveRecord::Migration
  def up
    drop_table :poderes
    drop_table :habilidades_opcionales
    drop_table :habilidades
    drop_table :atributos
    drop_table :fichas
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
