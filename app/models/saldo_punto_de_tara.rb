class SaldoPuntoDeTara < SaldoEspecifico

  attr_accessor :tara

  #(punto.tipo_de_punto.eql? :tipo_de_punto or punto.tipo_de_punto.eql? :punto_libre) and disponible?


  def descomponer! coleccion_de_saldos
    if @disponible
      saldos = []
      tara.valor.times do
        saldos.push Saldo.new :tipo_de_punto => :punto_libre, :disponible => true
      end
      coleccion_de_saldos.concat saldos
      coleccion_de_saldos.delete self
    end
  end
    
end
