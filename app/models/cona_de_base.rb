class ConaDeBase < ActiveRecord::Base
  belongs_to :arquetipo
  belongs_to :cona_de_catalogo


  def saldos
    resultado = []
    resultado.push SaldoEspecifico.new :tipo_de_punto => :punto_de_cona_especifica, :disponible => true, :tipo_especifico => cona_de_catalogo.nombre.to_sym
    resultado
  end


end
