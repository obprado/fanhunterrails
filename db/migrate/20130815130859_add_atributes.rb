class AddAtributes < ActiveRecord::Migration
  def initialize
    @att = [:combate,
            :artes_marciales,
            :defensa,
            :maestria,
            
            :disparo,
            :arrojar,
            :explosivos,
            :precision,
                                
            :musculos,
            :atletismo,
            :nadar,
            :supervivencia,
            
            :reflejos,
            :sigilo,
            :manitas,
            :conducir,
                                
            :neuronas,
            :investigar,
            :medicina,
            :culturilla,
            
            :carisma,
            :comecocos,
            :farolear,
            :idiomas,
            
            :agallas,
            :callejear,
            :intimidar,
            :crimen,
            
            :empatia,
            :observar,
            :psicologia,
            :aprendizaje_rapido]
  end
  
  def up    
    change_table 'characters' do |table|
      @att.each do |item|
        #     table.integer @att_and_habilities
        ##      @att_and_habilities.each do |item|
        table.integer item
        #   end
        # add_column 'characters', item, :integer
      end
    end
  end

  def down
    @att.each do |item|
      remove_column 'characters', item
    #remove_column 'characters', @att
    end
  end
end  
