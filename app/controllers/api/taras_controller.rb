class Api::TarasController < ApplicationController

  def show
    respond_with Tara.find params[:id]
  end

  def create
    @tara = Tara.new 
    @tara.save
    respond_with :api, @tara
  end

  def destroy
    Tara.find(params[:id]).destroy
    render nothing: true
  end


end
