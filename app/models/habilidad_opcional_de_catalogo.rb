class HabilidadOpcionalDeCatalogo < ActiveRecord::Base
  has_many :habilidades_opcionales
  belongs_to :atributo_de_catalogo
end
