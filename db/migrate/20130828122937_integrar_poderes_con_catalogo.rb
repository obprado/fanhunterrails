class IntegrarPoderesConCatalogo < ActiveRecord::Migration
  def up
    remove_column :poderes, :nombre
    remove_column :poderes, :coste
    change_table :poderes do |t|
      t.belongs_to :poder_de_catalogo
    end    
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
