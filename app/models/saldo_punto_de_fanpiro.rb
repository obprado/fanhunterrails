class SaldoPuntoDeFanpiro < Saldo

  def puede_pagar? punto
    (punto.tipo_de_punto.eql? :punto_de_poder or punto.tipo_de_punto.eql? :punto_de_atributo) and disponible?
  end

end
