class HabilidadOpcional < ActiveRecord::Base
  belongs_to :atributo
  belongs_to :habilidad_opcional_de_catalogo

  after_initialize :fill_defaults

  def fill_defaults
    unless self.nivel
      self.nivel = 0
    end
  end

  def nombre
    habilidad_opcional_de_catalogo.nombre
  end

  def gasto
    puntos = []
    nivel.times do
      puntos.push PuntoDeHabilidadOpcionalEspecifica.new :habilidad_opcional => self, :pagado => false
    end
    Gasto.new :puntos => puntos
  end
end
