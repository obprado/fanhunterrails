class Cona < ActiveRecord::Base
  belongs_to :narizon
  belongs_to :cona_de_catalogo

  def nombre
    cona_de_catalogo.nombre
  end

  def precio
    cona_de_catalogo.precio
  end

  def gasto
    puntos = []
    puntos.push PuntoDeConaEspecifica.new :cona => self, :pagado => false
    Gasto.new :puntos => puntos
  end


end
