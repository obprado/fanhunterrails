# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131002103829) do

  create_table "arquetipos", :force => true do |t|
    t.string   "nombre"
    t.integer  "puntos_de_atributo"
    t.integer  "puntos_de_habilidad"
    t.integer  "puntos_libres"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "atributos", :force => true do |t|
    t.integer  "nivel"
    t.integer  "atributo_de_catalogo_id"
    t.integer  "narizon_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalogo_de_atributos", :force => true do |t|
    t.string   "nombre"
    t.integer  "orden"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tipo_de_atributo_id"
    t.integer  "habilidad_gratuita_de_catalogo_id"
  end

  create_table "catalogo_de_conas", :force => true do |t|
    t.string   "nombre"
    t.integer  "precio"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalogo_de_habilidades", :force => true do |t|
    t.integer  "atributo_de_catalogo_id"
    t.string   "nombre"
    t.integer  "orden"
    t.boolean  "escala?"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalogo_de_habilidades_opcionales", :force => true do |t|
    t.integer  "atributo_de_catalogo_id"
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalogo_de_poderes", :force => true do |t|
    t.string   "nombre"
    t.integer  "precio"
    t.integer  "atributo_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "catalogo_de_taras", :force => true do |t|
    t.string   "nombre"
    t.integer  "valor"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conas", :force => true do |t|
    t.string   "especialidad"
    t.integer  "cona_de_catalogo_id"
    t.integer  "narizon_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conas_de_base", :force => true do |t|
    t.integer  "arquetipo_id"
    t.integer  "cona_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "habilidades", :force => true do |t|
    t.integer  "atributo_id"
    t.integer  "nivel"
    t.integer  "habilidad_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "habilidades_de_base", :force => true do |t|
    t.integer "arquetipo_id"
    t.integer "habilidad_de_catalogo_id"
    t.integer "nivel"
  end

  create_table "habilidades_opcionales", :force => true do |t|
    t.integer  "atributo_id"
    t.string   "especialidad"
    t.integer  "nivel"
    t.integer  "habilidad_opcional_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "habilidades_opcionales_de_base", :force => true do |t|
    t.integer  "arquetipo_id"
    t.integer  "habilidad_opcional_de_catalogo_id"
    t.integer  "nivel"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "narizones", :force => true do |t|
    t.string   "nombre"
    t.string   "jugador"
    t.integer  "arquetipo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poderes", :force => true do |t|
    t.integer  "nivel"
    t.integer  "narizon_id"
    t.integer  "poder_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taras", :force => true do |t|
    t.string   "especialidad"
    t.integer  "tara_de_catalogo_id"
    t.integer  "narizon_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taras_de_base", :force => true do |t|
    t.integer  "arquetipo_id"
    t.integer  "tara_de_catalogo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipos_de_atributo", :force => true do |t|
    t.string   "tipo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
