class Satisfaccion
  attr_accessor :saldos, :punto

  def initialize attributes = {:saldos => []}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def recuperar_puntos_de_deuda
    saldos.select { |saldo| saldo.deuda }
  end

end
