class Super < ArquetipoReal
  attr_accessor :arquetipo

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end
  
  def aplicar_modificadores
    PoderDeCatalogo.class_eval do
      def gasto
        puntos = []
     #   (precio/2).times { puntos.push PuntoLibreDePoder.new }
        puntos.push PuntoDePoder.new :precio => precio/2
        Gasto.new :puntos => puntos        
      end
    end  
  end
  
  def generar_saldos
    8.times do
      @arquetipo.saldos.push Saldo.new :tipo_de_punto => :punto_libre_de_poder, :disponible => true
    end
    @arquetipo.generar_saldos_helper
  end    

end


