class PoderDeCatalogo < ActiveRecord::Base
  belongs_to :atributo_de_catalogo
  has_many :poderes

  def to_s
    "#{nombre.titleize}  (#{precio})"
  end

  def gasto
    puntos = []
    puntos.push PuntoDePoder.new :poder_de_catalogo => self
    Gasto.new :puntos => puntos
  end

end
