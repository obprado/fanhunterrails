class ObrasEnBarnaCity < ActiveRecord::Migration
  def change
    remove_column :atributos, :tipo_de_atributo_id

    change_table :atributos do |t|
      t.belongs_to :narizon
    end

    drop_table :conas
    drop_table :taras
    
    create_table :catalogo_de_conas do |t|
      t.string 'nombre'
      t.integer 'precio'
    end    

    create_table :catalogo_de_taras do |t|
      t.string 'nombre'
      t.integer 'valor'
    end    

    

    create_table :conas do |t|
      t.string 'especialidad'
      t.belongs_to :cona_de_catalogo
      t.belongs_to :narizon
    end
    

    create_table :taras do |t|
      t.string 'especialidad'
      t.belongs_to :tara_de_catalogo
      t.belongs_to :narizon
    end
    
    
    change_table :catalogo_de_atributos do |t|
      t.belongs_to :tipo_de_atributo
    end

    remove_column :tipos_de_atributo, :narizon_id

#    remove_column :habilidades_opcionales, :arquetipo_id

    create_table :catalogo_de_habilidades do |t|
      t.belongs_to :atributo_de_catalogo
      t.string 'nombre'      
      t.integer 'orden'
      t.boolean 'escala?'
    end

    remove_column :habilidades, :arquetipo_id        
    remove_column :habilidades, 'nombre'

    
    change_table :habilidades do |t|
      t.belongs_to :habilidad_de_catalogo
    end

    create_table :catalogo_de_habilidades_opcionales do |t|
      t.belongs_to :atributo_de_catalogo
      t.string 'nombre'
    end

    remove_column :habilidades_opcionales, :arquetipo_id
    remove_column :habilidades_opcionales, 'nombre'


    change_table :habilidades_opcionales do |t|
      t.belongs_to :habilidad_opcional_de_catalogo
    end

    
  end
end
