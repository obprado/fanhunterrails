class PuntoDeConaEspecifica < PuntoEspecifico

  attr_accessor :cona

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = cona.nombre.to_sym
  end

  def descomponerse
    hijos = []
    cona.precio.times do
      hijos.push PuntoLibre.new 
    end
    hijos
  end
  
end
