class Ficha < ActiveRecord::Base
  has_many :atributos
  has_many :poderes, :through => :atributos
  accepts_nested_attributes_for :atributos

  def initialize
    @atributos_por_defecto = ['combate','disparo','musculos','reflejos','neuronas','carisma','agallas','empatia']
    super
  end

  def fill_defaults
    @atributos_por_defecto.each do |att|      
      attr = Atributo.new.fill_defaults att
      atributos.push attr
    end
  end

  def atributos_fisicos
    return atributos.select { |atributo| atributo.tipo.eql? ('fisico') }
  end

  def atributos_mentales
    return atributos.select { |atributo| atributo.tipo.eql? ('mental') }
  end
=begin
    @nombre = "fdsf"
    disparo = Atributo.new :nombre => 'disparo', :habilidad_opcional => 'precision', :tipo => 'fisico'
    disparo.habilidades.build :nombre => 'arrojar', :nivel => 0
    disparo.habilidades.build :nombre => 'explosivos', :nivel => 0

    musculos = Atributo.new :nombre => 'musculos', :habilidad_opcional => 'superviviencia', :tipo => 'fisico'
    musculos.habilidades.build :nombre => 'atletismo', :nivel => 0
    musculos.habilidades.build :nombre => 'nadar', :nivel => 0
    
    reflejos = Atributo.new :nombre => 'reflejos', :habilidad_opcional => 'conducir', :tipo => 'fisico'
    reflejos.habilidades.build :nombre => 'sigilo', :nivel => 0
    reflejos.habilidades.build :nombre => 'manitas', :nivel => 0
    
    neuronas = Atributo.new :nombre => 'neuronas', :habilidad_opcional => 'culturilla', :tipo => 'mental'
    neuronas.habilidades.build :nombre => 'investigar', :nivel => 0
    neuronas.habilidades.build :nombre => 'medicina', :nivel => 0
    
    carisma = Atributo.new :nombre => 'carisma', :habilidad_opcional => 'idioma', :tipo => 'mental'
    carisma.habilidades.build :nombre => 'comecocos', :nivel => 0
    carisma.habilidades.build :nombre => 'farolear', :nivel => 0
   
    agallas = Atributo.new :nombre => 'agallas', :habilidad_opcional => 'crimen', :tipo => 'mental'
    agallas.habilidades.build :nombre => 'callejear', :nivel => 0
    agallas.habilidades.build :nombre => 'intimidar', :nivel => 0
    
    empatia = Atributo.new :nombre => 'empatia', :habilidad_opcional => 'aprendizaje_rapido', :tipo => 'mental'
    empatia.habilidades.build :nombre => 'observar', :nivel => 0
    empatia.habilidades.build :nombre => 'psicologia', :nivel => 0
    atributos.push(combate) << disparo << musculos << reflejos << neuronas << carisma << agallas << empatia
  end
=end
end
