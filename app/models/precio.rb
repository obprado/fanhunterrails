class Precio

#  attr_accessor :tipo_de_punto, :cantidad
  attr_accessor :puntos

  def initialize(attributes = {})
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def saldos
    resultado = []
    puntos.each do |punto|
      resultado.push punto.saldos
    end
    resultado
  end

  def satisfacer_con monedero
    puntos.each do |punto|
      punto.satisfacer_con monedero
    end
  end
  
end
