class CreateCatalogoAtributos < ActiveRecord::Migration
  def change
    create_table :catalogo_de_atributos do |t|
      t.string :nombre
      t.integer :orden

      t.timestamps
    end
  end
end
