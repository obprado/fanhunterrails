json.array! @narizones do |narizon|
  json.(narizon, :id, :nombre, :jugador)
  if narizon.arquetipo
    json.arquetipo narizon.arquetipo.nombre
  end
end
