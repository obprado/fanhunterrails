class Cobrador
  
  attr_accessor :intentos
  
  def initialize
    
    @intentos = 
      [
       Intento.new({:tipo_de_saldo => :punto_de_atributo_especifico, :tipo_de_punto => :punto_de_atributo_especifico, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_atributo, :tipo_de_punto => :punto_de_atributo, :ultima_oportunidad => false}),

       Intento.new({:tipo_de_saldo => :punto_de_poder, :tipo_de_punto => :punto_de_poder, :ultima_oportunidad => false}),

       Intento.new({:tipo_de_saldo => :punto_de_fanpiro, :tipo_de_punto => :punto_de_poder, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_fanpiro, :tipo_de_punto => :punto_de_atributo, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_libre_de_poder_por_atributo, :tipo_de_punto => :punto_libre_de_poder_por_atributo, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_truco, :tipo_de_punto => :punto_libre_de_poder, :ultima_oportunidad => false}),

       Intento.new({:tipo_de_saldo => :punto_libre_de_poder, :tipo_de_punto => :punto_libre_de_poder, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_habilidad_especifica, :tipo_de_punto => :punto_de_habilidad_especifica, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_habilidad_opcional_especifica, :tipo_de_punto => :punto_de_habilidad_opcional_especifica, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_habilidad_opcional, :tipo_de_punto => :punto_de_habilidad_opcional, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_habilidad, :tipo_de_punto => :punto_de_habilidad, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_cona_especifica, :tipo_de_punto => :punto_de_cona_especifica, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_de_tara_especifica, :tipo_de_punto => :punto_de_tara_especifica, :ultima_oportunidad => true}),

       Intento.new({:tipo_de_saldo => :punto_libre, :tipo_de_punto => :punto_libre, :ultima_oportunidad => false})       
      ]

  end

  def realizar_cobros monedero, factura    
    @intentos.each do |intento|
      intento.realizar_cobro monedero, factura
    end
  end

end
