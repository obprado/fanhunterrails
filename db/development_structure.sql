CREATE TABLE "arquetipos" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "puntos_de_atributo" integer, "puntos_de_habilidad" integer, "puntos_libres" integer);
CREATE TABLE "atributos" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nivel" integer, "catalogo_atributos_id" integer);
CREATE TABLE "catalogo_atributos" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "orden" integer, "created_at" datetime, "updated_at" datetime, "tipos_de_atributo_id" integer);
CREATE TABLE "catalogo_de_conas" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "precio" integer);
CREATE TABLE "catalogo_de_habilidades" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "atributo_de_catalogo_id" integer, "nombre" varchar(255));
CREATE TABLE "catalogo_de_habilidades_opcionales" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "atributo_de_catalogo_id" integer, "nombre" varchar(255));
CREATE TABLE "catalogo_de_poderes" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "precio" integer, "atributo_de_catalogo_id" integer, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "catalogo_de_taras" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "valor" integer);
CREATE TABLE "conas" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "especialidad" varchar(255), "cona_de_catalogo_id" integer, "narizon_id" integer);
CREATE TABLE "habilidades" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "atributo_id" integer, "nivel" integer, "habilidad_de_catalogo_id" integer);
CREATE TABLE "habilidades_opcionales" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "atributo_id" integer, "especialidad" varchar(255), "nivel" integer, "habilidad_opcional_de_catalogo_id" integer);
CREATE TABLE "narizones" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nombre" varchar(255), "jugador" varchar(255), "arquetipo_id" integer);
CREATE TABLE "poderes" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "nivel" integer, "narizon_id" integer, "poder_de_catalogo_id" integer);
CREATE TABLE "schema_migrations" ("version" varchar(255) NOT NULL);
CREATE TABLE "taras" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "especialidad" varchar(255), "tara_de_catalogo_id" integer, "narizon_id" integer);
CREATE TABLE "tipos_de_atributo" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "tipo" varchar(255));
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
INSERT INTO schema_migrations (version) VALUES ('20130902145335');

INSERT INTO schema_migrations (version) VALUES ('20130827120030');

INSERT INTO schema_migrations (version) VALUES ('20130827074459');

INSERT INTO schema_migrations (version) VALUES ('20130819101901');

INSERT INTO schema_migrations (version) VALUES ('20130828092744');

INSERT INTO schema_migrations (version) VALUES ('20130822140929');

INSERT INTO schema_migrations (version) VALUES ('20130815130859');

INSERT INTO schema_migrations (version) VALUES ('20130819104917');

INSERT INTO schema_migrations (version) VALUES ('20130828095805');

INSERT INTO schema_migrations (version) VALUES ('20130819103127');

INSERT INTO schema_migrations (version) VALUES ('20130830104342');

INSERT INTO schema_migrations (version) VALUES ('20130822141254');

INSERT INTO schema_migrations (version) VALUES ('20130820151035');

INSERT INTO schema_migrations (version) VALUES ('20130821092801');

INSERT INTO schema_migrations (version) VALUES ('20130813105850');

INSERT INTO schema_migrations (version) VALUES ('20130819110428');

INSERT INTO schema_migrations (version) VALUES ('20130902102530');

INSERT INTO schema_migrations (version) VALUES ('20130823190653');

INSERT INTO schema_migrations (version) VALUES ('20130828122937');