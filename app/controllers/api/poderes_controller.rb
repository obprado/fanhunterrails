class Api::PoderesController < ApplicationController
  
  def index
    @poderes = Poder.all
    respond_with @poderes
  end

  def show
    respond_with Poder.find params[:id]
  end

  def create
    @poder = Poder.new
    @poder.save
    respond_with :api, @poder
  end

  def destroy
    Poder.find(params[:id]).destroy
    render nothing: true
  end

end
