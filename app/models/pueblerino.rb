class Pueblerino < ArquetipoReal

  def validaciones narizon = nil
    super
    limite_atributos_mentales narizon, 8
  end

  def limite_atributos_mentales pueblerino, cantidad
    suma_atributos_mentales = 0
    atributos_mentales = pueblerino.atributos.select { |atributo| atributo.atributo_de_catalogo.tipo_de_atributo.tipo.eql? "mental" }
    atributos_mentales.each { |atributo| suma_atributos_mentales += atributo.nivel}
    if suma_atributos_mentales > 8
      pueblerino.errors.add :el, "pueblerino no puede tener mas de #{cantidad} puntos en atributos mentales"
    end
  end

  def aplicar_modificadores
    PuntoDeConaEspecifica.class_eval do
      def descomponerse
        hijos = []
        precio_modificado = (cona.precio / 2.0).round
        precio_modificado.times do
          hijos.push PuntoLibre.new
        end
        hijos
      end
    end  
  end
    
end
