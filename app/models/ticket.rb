class Ticket
  
  attr_accessor :pagos

  def initialize attributes = {:pagos => []}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def puntos_por_pagar
    resultado = []
    pagos.each do |pago|
        resultado.concat pago.puntos_por_pagar
    end
    resultado
  end

end
