class Gasto
  attr_accessor :puntos, :concepto

  def initialize attributes = {:puntos => []}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def pagado?
    not puntos_por_pagar.empty?
  end

  def puntos_por_pagar
    puntos.select { |punto| not punto.pagado? }
  end

=begin
  def elegir_el_mejor saldos
    precios.select do |precio| 
      saldos_del_tipo = saldos.select { |saldo| saldo.tipo_de_punto.eql? precio.tipo_de_punto}
      saldos_del_tipo.size >= precio.cantidad      
    end.first
=end


  def paga monedero, factura
    satisfacciones = []
    puntos.each do |punto|
      satisfacciones.push Satisfaccion.new :saldos => monedero.saldos_para(punto, factura), :punto => punto
    end
    pago = Pago.new :gasto => self, :satisfacciones => satisfacciones
#    precio = elegir_el_mejor monedero.saldos_disponibles
#    pago = Pago.new :precio => precio, :gasto => self
#    pago = Pago.new :precio => precio, :saldos => monedero.saldos_de(precio), :gasto => self
#    monedero.perder! pago.saldos
    pago
  end

end
