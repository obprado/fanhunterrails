# -*- coding: utf-8 -*-
class ArquetipoReal  
  
  attr_accessor :arquetipo

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end
  
  def aplicar_modificadores
  end

  def validaciones narizon = nil
    limite narizon.atributos, 5, narizon
    limite narizon.habilidades, 5, narizon
    limite narizon.habilidades_opcionales, 5, narizon
  end

  def limite datos, maximo, narizon
    datos.each do |dato|
      if dato.nivel > maximo
        narizon.errors.add :te_has_pasado, "#{dato.nombre.titleize} no debería de estar por encima de #{maximo}"
      end
    end
  end
    
  def generar_saldos
    @arquetipo.generar_saldos_helper
  end    

end
