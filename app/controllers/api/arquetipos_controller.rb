class Api::ArquetiposController < ApplicationController

  def index
    @arquetipos = Arquetipo.all
    respond_with @arquetipos
  end

  def show
    @arquetipo = Arquetipo.find params[:id]
    respond_with @arquetipo
  end

end
