class Factura
  attr_accessor :puntos, :impagados
   
  def initialize attributes = {:puntos => [], :impagados => []}
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def cobrar monedero
    ticket = Ticket.new
    gastos.each do |gasto|
      pago = gasto.paga monedero, self
      ticket.pagos.push pago
    end
    ticket
  end

  def puntos_por_pagar
#    resultado = []
 #   gastos.each do |gasto|
  #    resultado.concat gasto.puntos_por_pagar
   # end
    #resultado
    #  impagados
    puntos.select { |punto| not punto.pagado }
  end

  def find tipo_de_punto
    @puntos.select { |punto| punto.tipo_de_punto.eql? tipo_de_punto and not punto.pagado?}
  end

  def descomponer! punto
    punto.descomponer! puntos
#    self.descontar! punto
  end

  def descontar! punto
    punto.pagar!
  end

end
