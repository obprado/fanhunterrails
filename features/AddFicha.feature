Feature: User can manually add a character
  In order accelerate game preparatives
  As a User
  I want to create a character

Scenario: Add a character
  Given I am on the "FanHunter" home page
  When I follow "add new character"
  Then I should be on the Create New Character page
  And I fill in "Name" with "Peter Petardo"
  And I select "super" from "archetype"
  And I press "create character"
  Then I should be on the FanHunter home page
  And I should see "Peter Petardo"
