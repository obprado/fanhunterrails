class Saldo

  attr_accessor :tipo_de_punto, :id, :disponible

  def initialize attributes = {:disponible => true}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    id = Saldo.siguiente_id
  end

  def disponible?
    @disponible
  end

  def gastar!
    @disponible = false
    self
  end
  
  def self.siguiente_id
    if @contador
      @contador = @contador + 1
    else
      @contador = 1
    end
  end

  def puede_pagar? punto
    (punto.tipo_de_punto.eql? tipo_de_punto) and disponible?
  end

  def descomponer! coleccion_de_puntos
  end

end
