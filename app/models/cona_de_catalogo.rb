class ConaDeCatalogo < ActiveRecord::Base
  has_many :conas

  def to_s
    "#{nombre.humanize} (#{precio})"
  end

  def to_sym
    nombre.titleize.delete(" ").underscore.to_sym
  end


end
