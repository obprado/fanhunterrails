class Api::NarizonesController < ApplicationController

  respond_to :json
  
  def narizon_params
    params#.require(:narizon).permit!# :nombre, :atributos, :habilidades, {:arquetipo => [:id, :nombre]}, :jugador, :id, {:poderes_attributes => [:id]}, :taras, :conas
  end
  
  def index
    @narizones = Narizon.all
  end

  def new
    @narizon = Narizon.new
    @narizon.fill_defaults
    @narizon.ordenar
    @narizon
  end

  def create
    @narizon = Narizon.new
    @narizon.fill_defaults
    @narizon.ordenar
    @narizon.save
  end

  def show
    @narizon = Narizon.find(params[:id])
    @disabled = true
    @narizon.ordenar
#    render :json => @narizon.to_json(:include => [:atributos => {:include => :atributo_de_catalogo}, :habilidades] )
  end

  def update
    @narizon = Narizon.find params[:id]
#    if not @narizon.arquetipo.nombre.eql? "cadaver"
 #     @narizon.pagar
  #  end
    @narizon = Narizon.hash_to_narizon narizon_params, @narizon
    if @narizon.save #update_attributes narizon_params
      render nothing: true
      #redirect_to narizones_path
    else
      #TODO Return 400 Bad Request
#      @narizon.refill
 #     render "edit"
    end    
  end

  def destroy
    @narizon = Narizon.find(params[:id])
    @narizon.destroy
    render nothing: true
    #TODO return 200 OK
  end

  def validate
    narizon = Narizon.new
    narizon.fill_defaults
    narizon.ordenar
    @narizon = Narizon.hash_to_narizon narizon_params, narizon
#    @narizon = Narizon.find(params[:id])
    @narizon.pagar    
  end
end
