# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ nombre: 'Chicago' }, { nombre: 'Copenhagen' }])
#   Mayor.create(nombre: 'Emanuel', city: cities.

tipos_de_atributo = [
                     {:tipo => 'fisico'},
                     {:tipo => 'mental'}
                    ]

#TipoDeAtributo.delete_all
if TipoDeAtributo.all.empty?
  tipos_de_atributo.each do |tipo|
    TipoDeAtributo.create! tipo
  end
end


atributos = [{:nombre => "combate", :orden => 1, :tipo => 'fisico'},
             {:nombre => "disparo", :orden => 2, :tipo => 'fisico'},
             {:nombre => "musculos", :orden => 3, :tipo => 'fisico'},
             {:nombre => "reflejos", :orden => 4, :tipo => 'fisico'},
             {:nombre => "neuronas", :orden => 5, :tipo => 'mental'},
             {:nombre => "carisma", :orden => 6, :tipo => 'mental'},
             {:nombre => "agallas", :orden => 7, :tipo => 'mental'},
             {:nombre => "empatia", :orden => 8, :tipo => 'mental'}
            ]

#AtributoDeCatalogo.delete_all
if AtributoDeCatalogo.all.empty?
  atributos.each do |params|
    atributo = AtributoDeCatalogo.new
    atributo.nombre = params[:nombre]
    atributo.orden = params[:orden]
    atributo.tipo_de_atributo = TipoDeAtributo.find_by_tipo params[:tipo]
    atributo.save!
  end
end

poderes = {

"combate" => [
  {:nombre => "armadura", :precio => 6}, 
  {:nombre => "golpe_letal", :precio => 4},
  {:nombre => "toque", :precio => 4},
  {:nombre => "absorcion", :precio => 4}],

"disparo" => [
  {:nombre => "control", :precio => 6},
  {:nombre => "rayo", :precio => 4},
  {:nombre => "multirrayo", :precio => 10},
  {:nombre => "vision_rayos_x", :precio => 4},
  {:nombre => "onda_de_choque", :precio => 4}],

"musculos" => [
  {:nombre => "regeneracion", :precio => 8},
  {:nombre => "inmunidad", :precio => 10},
  {:nombre => "resistencia", :precio => 4},
  {:nombre => "potencia", :precio => 10},
  {:nombre => "replicacion", :precio => 6}],

"reflejos" => [
  {:nombre => "trepamuros", :precio => 4},
  {:nombre => "supermovimiento", :precio => 4},
  {:nombre => "celeridad", :precio => 8},
  {:nombre => "borrosidad", :precio => 6},
  {:nombre => "agilidad", :precio => 4}],

"neuronas" => [
  {:nombre => "invisibilidad", :precio => 8},
  {:nombre => "campo_de_fuerza", :precio => 4},
  {:nombre => "curacion", :precio => 4},
  {:nombre => "oscuridad", :precio => 6},
  {:nombre => "invocacion", :precio => 8},
  {:nombre => "anulacion", :precio => 10}],

"carisma" => [
  {:nombre => "ilusion", :precio => 4},
  {:nombre => "presencia", :precio => 8},
  {:nombre => "maldicion", :precio => 6},
  {:nombre => "control_memoria", :precio => 10}],

"agallas" => [
  {:nombre => "incorporeidad", :precio => 6},
  {:nombre => "multiformidad", :precio => 4},
  {:nombre => "teleport", :precio => 6},
  {:nombre => "teleport_X", :precio => 8},
  {:nombre => "control_tiempo", :precio => 10}],

"empatia" => [
  {:nombre => "control_mental", :precio => 6},
  {:nombre => "telepatia", :precio => 4},
  {:nombre => "deteccion_avanzada", :precio => 4},
  {:nombre => "telekinesis", :precio => 6},
  {:nombre => "precognicion", :precio => 6},
  {:nombre => "control_suerte", :precio => 4}]
}

#PoderDeCatalogo.delete_all
if PoderDeCatalogo.all.empty?
  atributos.each do |params|
    poderes[params[:nombre]].each do |poder|
      poderNuevo = PoderDeCatalogo.create! poder
      poderNuevo.atributo_de_catalogo = AtributoDeCatalogo.select { |x| x.nombre.eql?(params[:nombre])  }.first 
      poderNuevo.save!
    end
  end
end

habilidades = [
  { :nombre => 'combate', :habilidad_opcional => 'maestria', :habilidad1 => {:nombre => 'artes_marciales', :escala? => false}, :habilidad2 => {:nombre => 'defensa', :escala? => true}},
  { :nombre => 'disparo', :habilidad_opcional => 'precision', :habilidad1 => {:nombre => 'arrojar', :escala? => true}, :habilidad2 => {:nombre => 'explosivos'}, :escala? => false},
  { :nombre => 'musculos', :habilidad_opcional => 'supervivencia', :habilidad1 => {:nombre => 'atletismo', :escala? => true}, :habilidad2 => {:nombre => 'nadar'}, :escala? => false},
  { :nombre => 'reflejos', :habilidad_opcional => 'conducir', :habilidad1 => {:nombre => 'sigilo', :escala? => true}, :habilidad2 => {:nombre => 'manitas'}, :escala? => false},
  
  { :nombre => 'neuronas', :habilidad_opcional => 'culturilla', :habilidad1 => {:nombre => 'investigar', :escala? => true}, :habilidad2 => {:nombre => 'medicina'}, :escala? => false},
  { :nombre => 'carisma', :habilidad_opcional => 'idioma', :habilidad1 => {:nombre => 'comecocos', :escala? => true}, :habilidad2 => {:nombre => 'farolear'}, :escala? => false},
  { :nombre => 'agallas', :habilidad_opcional => 'crimen', :habilidad1 => {:nombre => 'callejear', :escala? => true}, :habilidad2 => {:nombre => 'intimidar'}, :escala? => false},
  { :nombre => 'empatia', :habilidad_opcional => 'aprendizaje_rapido', :habilidad1 => {:nombre => 'observar', :escala? => true}, :habilidad2 => {:nombre => 'psicologia'}, :escala? => false}
]

#HabilidadDeCatalogo.delete_all
if HabilidadDeCatalogo.all.empty?
  habilidades.each do |habilidad|
    habilidadNueva1 = HabilidadDeCatalogo.new habilidad[:habilidad1]
    habilidadNueva2 = HabilidadDeCatalogo.new habilidad[:habilidad2]
    habilidadOpcional = HabilidadOpcionalDeCatalogo.new :nombre => habilidad[:habilidad_opcional]

    atributo = AtributoDeCatalogo.find_by_nombre habilidad[:nombre]

    habilidadNueva1.atributo_de_catalogo = atributo
    habilidadNueva2.atributo_de_catalogo = atributo
    habilidadOpcional.atributo_de_catalogo = atributo 

    atributo.catalogo_de_habilidades.push habilidadNueva1
    atributo.catalogo_de_habilidades.push habilidadNueva2
    atributo.habilidad_opcional_de_catalogo = habilidadOpcional
    habilidadNueva1.orden = 1
    habilidadNueva2.orden = 2    

    habilidadNueva1.save!
    habilidadNueva2.save!
    habilidadOpcional.save!
  end
end

arquetipos = [
              {:nombre => "rebelde", :puntos_de_atributo => 20, :puntos_de_habilidad => 16, :puntos_libres => 8},
              {:nombre => "super", :puntos_de_atributo => 20, :puntos_de_habilidad => 0, :puntos_libres => 0},
              {:nombre => "fanpiro", :puntos_de_atributo => 24, :puntos_de_habilidad => 10, :puntos_libres => 8},
              {:nombre => "espia", :puntos_de_atributo => 20, :puntos_de_habilidad => 20, :puntos_libres => 10},
              {:nombre => "tecnico", :puntos_de_atributo => 20, :puntos_de_habilidad => 12, :puntos_libres => 6},
              {:nombre => "cadaver", :puntos_de_atributo => 20, :puntos_de_habilidad => 0, :puntos_libres => 0},
              {:nombre => "negociador", :puntos_de_atributo => 20, :puntos_de_habilidad => 0, :puntos_libres => 6},
              {:nombre => "taxista", :puntos_de_atributo => 20, :puntos_de_habilidad => 6, :puntos_libres => 6},
              {:nombre => "mostruo", :puntos_de_atributo => 32, :puntos_de_habilidad => 0, :puntos_libres => 0},
              {:nombre => "pueblerino", :puntos_de_atributo => 20, :puntos_de_habilidad => 6, :puntos_libres => 8},
              {:nombre => "mentalista", :puntos_de_atributo => 20, :puntos_de_habilidad => 6, :puntos_libres => 6},
              {:nombre => "fan_knight", :puntos_de_atributo => 24, :puntos_de_habilidad => 6, :puntos_libres => 10}
]

#Arquetipo.delete_all
if Arquetipo.all.empty?
  arquetipos.each do |arquetipo|
    Arquetipo.create! arquetipo
  end
end

taras = [ 
 { :nombre => "coleguilla", :valor => 3 },
 { :nombre => "coleguilla_grupo", :valor => 1 },
 { :nombre => "sordera", :valor => 2 },
 { :nombre => "cegato", :valor => 3 },
 { :nombre => "manco", :valor => 4 },
 { :nombre => "tuerto", :valor => 3 },
 { :nombre => "cojo", :valor => 4 },
 { :nombre => "mudo", :valor => 4 },
 { :nombre => "sordo", :valor => 8 },
 { :nombre => "alergia", :valor => 2 },
 { :nombre => "exceso_de_velocidad", :valor => 2 },
 { :nombre => "fobia", :valor => 2 },
 { :nombre => "pobre", :valor => 2 },
 { :nombre => "rebotado", :valor => 2 },
 { :nombre => "paranoia", :valor => 2 },
 { :nombre => "codigo_de_honor", :valor => 2 },
 { :nombre => "codigo_inquebrantable", :valor => 4 },
 { :nombre => "archienemigo", :valor => 3 },
 { :nombre => "nemesis", :valor => 12 },
 { :nombre => "cagao", :valor => 3 },
 { :nombre => "cruce_de_cables", :valor => 3 },
 { :nombre => "estigma", :valor => 3 },
 { :nombre => "estirado", :valor => 3 },
 { :nombre => "gafe", :valor => 3 },
 { :nombre => "pacto_3", :valor => 3 },
 { :nombre => "pacto_6", :valor => 6 },
 { :nombre => "pacto_9", :valor => 9 },
 { :nombre => "pacto_12", :valor => 12 },
 { :nombre => "obseso_sesua", :valor => 3 },
 { :nombre => "perseguido_3", :valor => 3 },
 { :nombre => "perseguido_6", :valor => 6 },
 { :nombre => "perseguido_9", :valor => 9 },
 { :nombre => "secreto_weirdo", :valor => 3 },
 { :nombre => "sensible_a_la_luz", :valor => 4 },
 { :nombre => "distraido", :valor => 4 },
 { :nombre => "debilidad_4", :valor => 4 },
 { :nombre => "debilidad_8", :valor => 8 },
 { :nombre => "debilidad_12", :valor => 12 },
 { :nombre => "enfermizo", :valor => 4 },
 { :nombre => "vicio", :valor => 4 },
 { :nombre => "enemigo_publico_macutes", :valor => 4 },
 { :nombre => "enemigo_publico_fh", :valor => 8 },
 { :nombre => "enemigo_publico_fh_de_asalto", :valor => 12 },
 { :nombre => "fallos_racord_4", :valor => 4 },
 { :nombre => "fallos_racord_8", :valor => 8 },
 { :nombre => "fallos_racord_12", :valor => 12 },
 { :nombre => "esquizofrenia", :valor => 5 },
 { :nombre => "torpe", :valor => 6 },
 { :nombre => "cenizo", :valor => 6 },
 { :nombre => "endeudado", :valor => 6 },
 { :nombre => "gigantismo", :valor => 6 },
 { :nombre => "olvidadizo", :valor => 6 },
 { :nombre => "licantropismo", :valor => 8 },
 { :nombre => "sobrenatural", :valor => 8 },
 { :nombre => "reposado", :valor => 12 },
 { :nombre => "atontado", :valor => 12 }
]


if TaraDeCatalogo.all.empty?
  taras.each do |tara|
    TaraDeCatalogo.create! tara
  end
end

conas =
[
 { :nombre => "Sex appeal", :precio => 1 },
 { :nombre => "Ambidiestro", :precio => 1 },
 { :nombre => "Empatia animal", :precio => 1 },
 { :nombre => "Insomne", :precio => 1 },
 { :nombre => "Inmenzo", :precio => 2 },
 { :nombre => "Conexiones", :precio => 2 },
 { :nombre => "Contactos", :precio => 2 },
 { :nombre => "Machote", :precio => 2 },
 { :nombre => "Ojos Coloraos", :precio => 2 },
 { :nombre => "Memoria fotografica", :precio => 2 },
 { :nombre => "Sentido comun", :precio => 2 },
 { :nombre => "Sentido desarrollado", :precio => 2 },
 { :nombre => "Simpatico", :precio => 2 },
 { :nombre => "Amigotes", :precio => 3 },
 { :nombre => "Herencia", :precio => 3 },
 { :nombre => "Duro", :precio => 3 },
 { :nombre => "Miembro extra", :precio => 3 },
 { :nombre => "Labia", :precio => 3 },
 { :nombre => "Veterano de guerra", :precio => 3 },
 { :nombre => "Sangre acida", :precio => 3 },
 { :nombre => "Golpe de cirujano", :precio => 5 },
 { :nombre => "NOOOOO", :precio => 6 },
 { :nombre => "Enciclopedia viviente", :precio => 6 },
 { :nombre => "Mucha chicha", :precio => 6 },
 { :nombre => "Pirata", :precio => 6 },
 { :nombre => "Sangre fria", :precio => 6 },
 { :nombre => "Sangre espesa", :precio => 6 },
 { :nombre => "Suerte", :precio => 6 },
 { :nombre => "McGuiver", :precio => 8 },
 { :nombre => "Primer", :precio => 8 },
 { :nombre => "Pastoso", :precio => 12 },
 { :nombre => "Samurai", :precio => 12 },
 { :nombre => "Inmortal", :precio => 12 },
 { :nombre => "Protagonista de la pelicula", :precio => 12 },
 { :nombre => "Canalizar", :precio => 12 },
 { :nombre => "Chuck esta con nosotros", :precio => 12 },
 { :nombre => "Protoheroe", :precio => 12 },
 { :nombre => "Super Dotado", :precio => 12 },
]

if ConaDeCatalogo.all.empty?
  conas.each do |cona|
    ConaDeCatalogo.create! cona
  end
end

atributos_con_habilidad_gratuita = [{:atributo => "combate", :habilidad => "defensa"},
                                    {:atributo => "disparo", :habilidad => "arrojar"},
                                    {:atributo => "musculos", :habilidad => "atletismo"},
                                    {:atributo => "reflejos", :habilidad => "sigilo"},
                                    {:atributo => "neuronas", :habilidad => "investigar"},
                                    {:atributo => "carisma", :habilidad => "comecocos"},
                                    {:atributo => "agallas", :habilidad => "callejear"},
                                    {:atributo => "empatia", :habilidad => "observar"}
                                   ]
unless AtributoDeCatalogo.find_by_nombre("combate").habilidad_gratuita
  atributos_con_habilidad_gratuita.each do |relacion|
    atributo = AtributoDeCatalogo.find_by_nombre relacion[:atributo]
    habilidad = HabilidadDeCatalogo.find_by_nombre relacion[:habilidad]
    atributo.habilidad_gratuita = habilidad
    atributo.save!
  end
end

caracteristicas_de_base = 
[
 { :arquetipo => "fanpiro", :conas => ["Ojos Coloraos"], :taras => ["enemigo_publico_fh", "vicio"], :habilidades => [], :habilidades_opcionales => [] },
 { :arquetipo => "espia", :conas => ["Contactos"], :taras => [], :habilidades => [], :habilidades_opcionales => [] },
 { :arquetipo => "tecnico", :conas => ["Conexiones"], :taras => [], :habilidades => [], :habilidades_opcionales => [] },
 { :arquetipo => "negociador", :conas => ["Sentido comun", "Labia"], :taras => ["archienemigo"], 
   :habilidades => [ 
                    { :nombre => "callejear", :nivel => 2 },  
                    { :nombre => "psicologia", :nivel => 2 },  
                    { :nombre => "farolear", :nivel => 3 },  
                    { :nombre => "comecocos", :nivel => 3 }, 
                    { :nombre => "intimidar", :nivel => 3 },  
                    { :nombre => "observar", :nivel => 2 }
                   ], 
   :habilidades_opcionales => [] },
 
{ :arquetipo => "taxista", :conas => ["Veterano de guerra"], :taras => ["rebotado", "exceso_de_velocidad"], 
   :habilidades => [
                    { :nombre => "arrojar", :nivel => 2 },  
                    { :nombre => "manitas", :nivel => 2 }  
                   ], 
   :habilidades_opcionales => [
                               { :nombre => "conducir", :nivel => 4 },  
                               { :nombre => "crimen", :nivel => 2 },  
                               { :nombre => "culturilla", :nivel => 4 }                                 
                              ] },
 { :arquetipo => "mostruo", :conas => [], :taras => ["sobrenatural"], :habilidades => [], :habilidades_opcionales => [] },
 { :arquetipo => "pueblerino", :conas => [], :taras => [], 
   :habilidades => [
                    { :nombre => "atletismo", :nivel => 3 },  
                    { :nombre => "observar", :nivel => 3 },  
                    { :nombre => "sigilo", :nivel => 2 },  
                    { :nombre => "nadar", :nivel => 3 },  
                    { :nombre => "medicina", :nivel => 2 }
                   ], 
   :habilidades_opcionales => [] },
 { :arquetipo => "mentalista", :conas => [], :taras => [], :habilidades => [], :habilidades_opcionales => [
                                                                                                           {:nombre => "culturilla", :nivel => "4"}] },
 { :arquetipo => "fan_knight", :conas => [], :taras => ["codigo_de_honor", "archienemigo"], 
   :habilidades => [
                    { :nombre => "defensa", :nivel => 2 },  
                    { :nombre => "atletismo", :nivel => 2 },  
                    { :nombre => "observar", :nivel => 2 }  
                   ], 
   :habilidades_opcionales => [
                               { :nombre => "maestria", :nivel => 3 }
                              ] }
]

if ConaDeBase.all.empty?
  caracteristicas_de_base.each do |caracteristica|
    arquetipo = Arquetipo.find_by_nombre caracteristica[:arquetipo]
    
    caracteristica[:conas].each do |cona_nombre|
      cona = ConaDeCatalogo.find_by_nombre cona_nombre
      cona_base = ConaDeBase.new
      cona_base.arquetipo = arquetipo
      cona_base.cona_de_catalogo = cona
      cona_base.save!
    end
    
    caracteristica[:taras].each do |tara_nombre|
      tara = TaraDeCatalogo.find_by_nombre tara_nombre
      tara_base = TaraDeBase.new
      tara_base.arquetipo = arquetipo
      tara_base.tara_de_catalogo = tara
      tara_base.save!
    end
    
    caracteristica[:habilidades].each do |habilidad|
      habilidad_de_catalogo = HabilidadDeCatalogo.find_by_nombre habilidad[:nombre]
      habilidad_base = HabilidadDeBase.new :nivel => habilidad[:nivel]
      habilidad_base.arquetipo = arquetipo
      habilidad_base.habilidad_de_catalogo = habilidad_de_catalogo
      habilidad_base.save!
    end    
    
    caracteristica[:habilidades_opcionales].each do |habilidad_opcional|
      habilidad_opcional_de_catalogo = HabilidadOpcionalDeCatalogo.find_by_nombre habilidad_opcional[:nombre]
      habilidad_opcional_base = HabilidadOpcionalDeBase.new :nivel => habilidad_opcional[:nivel]
      habilidad_opcional_base.arquetipo = arquetipo
      habilidad_opcional_base.habilidad_opcional_de_catalogo = habilidad_opcional_de_catalogo
      habilidad_opcional_base.save!
    end
    
  end
end
