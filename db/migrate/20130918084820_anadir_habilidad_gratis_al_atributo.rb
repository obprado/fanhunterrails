class AnadirHabilidadGratisAlAtributo < ActiveRecord::Migration

  def change
    change_table :catalogo_de_atributos do |t|
      t.references :habilidad_gratuita_de_catalogo
    end
  end

end
