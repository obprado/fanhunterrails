class PuntoDeHabilidadOpcionalEspecifica < PuntoEspecifico

  attr_accessor :habilidad_opcional

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = habilidad_opcional.nombre.to_sym
  end

  def nombre
    habilidad_opcional.nombre
  end
  
  def descomponerse
    hijos = []
    hijos.push PuntoDeHabilidadOpcional.new 
    hijos
  end
  
end
