require 'spec_helper'

describe Monedero do 
  describe 'saldos_disponibles' do
    it 'deberia devolver los saldos disponibles' do
      saldo1 = Saldo.new :tipo_de_punto => :punto_de_atributo, :disponible => true
      saldo2 = Saldo.new :tipo_de_punto => :punto_de_atributo, :disponible => false
      saldo3 = Saldo.new :tipo_de_punto => :punto_libre, :disponible => true
      saldo4 = Saldo.new :tipo_de_punto => :punto_de_habilidad, :disponible => false
      saldo5 = Saldo.new :tipo_de_punto => :punto_de_deuda, :disponible => true
      saldo6 = Saldo.new
      monedero = Monedero.new [saldo1, saldo2, saldo3, saldo4, saldo5, saldo6]
      expect(monedero.saldos_disponibles).to eq([saldo1, saldo3, saldo5, saldo6])
    end    
  end

  describe 'saldos_de' do
    it 'deberia devolver los saldos que mejor satisfagan el precio' do
      
    end
  end
end
