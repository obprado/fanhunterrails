class AnadirHabilidadesAlArquetipo < ActiveRecord::Migration

  def change

    create_table :habilidades_de_base do |t|
      t.belongs_to :arquetipo
      t.belongs_to :habilidad_de_catalogo
      t.integer "nivel"
    end


    create_table :habilidades_opcionales_de_base do |t|
      t.belongs_to :arquetipo
      t.belongs_to :habilidad_opcional_de_catalogo
      t.integer "nivel"
    end

  end

end
