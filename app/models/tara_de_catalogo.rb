class TaraDeCatalogo < ActiveRecord::Base
  has_many :taras_de_catalogo

  def to_s
    "#{nombre.humanize} (#{valor})"
  end

end
