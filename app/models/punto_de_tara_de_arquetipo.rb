class PuntoDeTaraDeArquetipo < PuntoEspecifico

  attr_accessor :tara

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = tara.nombre.to_sym
  end

  def to_s
    tara.nombre.to_s.titleize
  end

  def  puede_descomponerse?
    false
  end

  def tipo_de_punto
    :punto_de_tara_especifica
  end

end
