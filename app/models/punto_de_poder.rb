class PuntoDePoder < Punto

  attr_accessor :poder_de_catalogo

  def initialize attributes = {}
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

    
  def descomponerse
    hijos = []
    poder_de_catalogo.precio.times do 
      hijos.push PuntoLibreDePoderPorAtributo.new :poder_de_catalogo => poder_de_catalogo, :pagado => false
    end
    hijos
  end

end
