class HabilidadOpcionalDeBase < ActiveRecord::Base
  belongs_to :arquetipo
  belongs_to :habilidad_opcional_de_catalogo


  def saldos
    resultado = []
    resultado.push SaldoEspecifico.new :tipo_de_punto => :punto_de_habilidad_opcional_especifica, :disponible => true, :tipo_especifico => habilidad_opcional_de_catalogo.nombre.to_sym
    resultado
  end

  
end
