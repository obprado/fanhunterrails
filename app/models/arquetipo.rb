require 'forwardable'

class Arquetipo < ActiveRecord::Base 
  extend Forwardable

  has_many :narizones
  has_many :conas_de_base
  has_many :habilidades_de_base
  has_many :habilidades_opcionales_de_base
  has_many :taras_de_base

  attr_accessor :saldos, :arquetipo_real

  after_initialize :iniciar

#  validate :validaciones

  def iniciar
    @arquetipo_real = nombre.classify.constantize.new 
    @arquetipo_real.arquetipo = self
    @saldos = []
  end

  def_delegators :@arquetipo_real, :generar_saldos, :aplicar_modificadores, :validaciones

  def generar_saldos_helper
    habilidades_opcionales_de_base.each do |habilidad|
      habilidad.nivel.times do
        @saldos.concat habilidad.saldos
      end
    end

    habilidades_de_base.each do |habilidad|
      habilidad.nivel.times do
        @saldos.concat habilidad.saldos
      end
    end

    conas_de_base.each do |cona|
      @saldos.concat cona.saldos
    end
    
    puntos_de_atributo.times do
      @saldos.push Saldo.new :tipo_de_punto => :punto_de_atributo, :disponible => true
    end
    
    puntos_de_habilidad.times do
      @saldos.push Saldo.new :tipo_de_punto => :punto_de_habilidad, :disponible => true
    end
      
    puntos_libres.times do    
      @saldos.push Saldo.new :tipo_de_punto => :punto_libre, :disponible => true
    end
       
    @saldos
  end
  
  def nombre_formulario
    nombre.titleize
  end

  def gastos
    puntos_de_gasto = []
    taras_de_base.each do |tara_de_base|
      puntos_de_gasto.concat tara_de_base.gasto
    end
    puntos_de_gasto
  end

end
