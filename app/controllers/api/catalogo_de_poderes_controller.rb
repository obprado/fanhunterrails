class Api::CatalogoDePoderesController < ApplicationController

  def index
    @poderes = PoderDeCatalogo.all
    respond_with @poderes
  end

  def show
    @poder = PoderDeCatalogo.find params[:id]
    respond_with @poder
  end

end
