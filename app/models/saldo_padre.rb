class SaldoPadre

  attr_accessor :hijos

  def initialize attributes = {:hijos => []}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def gastar!
    hijos.each do |hijo|
      hijo.disponible = false
    end
  end

end
