class HabilidadDeCatalogo < ActiveRecord::Base
  has_many :habilidades
  belongs_to :atributo_de_catalogo


  def to_sym
    nombre.titleize.delete(" ").to_sym
  end


end
