class Monedero
  attr_accessor :saldos
  
  def initialize attributes = {}             
    attributes.each do |nombre, valor|       
      send("#{nombre}=", valor)              
    end                                      
  end        

  def descomponer! saldo
    saldo.descomponer! @saldos
  end

  def desembolsar! saldo
    saldo.gastar!
  end

  def saldos_disponibles
    @saldos.select {|saldo| saldo.disponible?}
  end


  def pagar! punto
    punto.pagar!
    @saldos.select { |saldo| saldo.puede_pagar? punto }.first.gastar!
  end


  def saldos_para punto, factura
    if puede_pagar punto
      [pagar!(punto)]
    else
      if punto.puede_descomponerse?
        resultado = []
        punto.descomponer.each do |punto_hijo|
          if puede_pagar(punto_hijo) or punto_hijo.puede_descomponerse?
            resultado.concat(saldos_para(punto_hijo, factura))
          else
            factura.impagados.push punto_hijo
          end
        end
        resultado
      end
    end     
  end
  
  def puede_pagar punto
    not (@saldos.select { |saldo| saldo.puede_pagar? punto }.empty?)
  end

  def find tipo_de_saldo
    @saldos.select { |saldo| saldo.tipo_de_punto.eql? tipo_de_saldo and saldo.disponible?}
  end

end
