class TipoDeAtributo < ActiveRecord::Base
  belongs_to :narizon
  has_many :catalogo_de_atributos

=begin  
  has_many :poderes, :through => :atributos
  has_many :taras, :through => :atributos
  has_many :conas, :through => :atributos
=end
#  accepts_nested_attributes_for :cataatributos

  def ordenar 
    atributos_orden = {'combate' => 1,'disparo' => 2, 'musculos' => 3,'reflejos' => 4, 'neuronas' => 5,'carisma' => 6,'agallas' => 7,'empatia' => 8}
    self.atributos.sort! do |a,b|
      atributos_orden[a.nombre] <=> atributos_orden[b.nombre]
    end
  end
      

  def fill_defaults
    @atributos_default = {
      'fisico' => ['combate','disparo', 'musculos','reflejos'] ,
      'mental' => ['neuronas','carisma','agallas','empatia']
    }
    @atributos_default[tipo].each do |atributo|
      atributos.build(:nombre => atributo).fill_defaults
    end
  end
end
