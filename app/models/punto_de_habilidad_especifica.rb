class PuntoDeHabilidadEspecifica < PuntoEspecifico

  attr_accessor :habilidad

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = habilidad.nombre.to_sym
  end

  def nombre
    habilidad.nombre
  end
  
  def descomponerse
    hijos = []
    hijos.push PuntoDeHabilidad.new 
    hijos
  end
  
end
