json.(@narizon.monedero, :saldos)
json.factura @narizon.factura.puntos_por_pagar do |punto|
  json.tipo_de_punto punto.class.name.underscore
  json.(punto, :pagado)
end
