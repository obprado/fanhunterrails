class Intento
  attr_accessor :tipo_de_saldo, :tipo_de_punto, :ultima_oportunidad

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
  end

  def realizar_cobro monedero, factura
    puntos = factura.find @tipo_de_punto
    saldos = monedero.find @tipo_de_saldo

    puntos.each do |punto|
      saldos_candidatos = saldos.select { |saldo| saldo.puede_pagar? punto }
      unless saldos_candidatos.empty?
        monedero.desembolsar! saldos_candidatos.first
        factura.descontar! punto
      end
    end

    saldos_sobrantes = saldos.select { |saldo| saldo.disponible? }
    puntos_sobrantes = puntos.select { |punto| not punto.pagado? }


    saldos_sobrantes.each do |saldo|
      monedero.descomponer! saldo
    end


    if @ultima_oportunidad
      puntos_sobrantes.each do |punto|
        if punto.puede_descomponerse?
          factura.descomponer! punto
        end
      end
    end

  end

end
