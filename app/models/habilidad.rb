# -*- coding: utf-8 -*-
class Habilidad < ActiveRecord::Base
  belongs_to :atributo, :inverse_of => :habilidades
  belongs_to :habilidad_de_catalogo

  after_initialize :fill_defaults

  def fill_defaults
    unless self.nivel
      self.nivel = 0
    end
  end

  def escala?
    habilidad_de_catalogo.escala?
  end

  def nombre
    habilidad_de_catalogo.nombre
  end

  def gasto
    puntos = []
    nivel.times do
      puntos.push PuntoDeHabilidadEspecifica.new :habilidad => self, :pagado => false
    end
    Gasto.new :puntos => puntos
  end
end
