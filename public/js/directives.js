var fhDirectives = angular.module('fhDirectives', ['ngRoute','config']);


fhDirectives.filter('humanize', function(){
    return function(text) {
	if(text) { 
	    var string = text.split("_").join(" ").toLowerCase();
	    string = string.charAt(0).toUpperCase() + string.slice(1); 
	    return string
	};
    };
});


fhDirectives.filter('active', function(){
    return function(activo){
	return activo ? 'active' : '';
    }
});

fhDirectives.filter('in_range', function(){
    return function(taras, rango){ 
	if (!rango){return taras};
	selected = [];
	taras.forEach(function(tara){
	    if (tara.valor >= rango.minimo && tara.valor <= rango.maximo){ selected.push(tara);  }
	});
/*	for (var i = 0; i < taras.length; i++) {
	    if (taras[i].valor >= rango.minimo && taras[i].valor <= rango.maximo){ selected.push(taras[i]);  }
	}*/
	return selected;
    };
});

fhDirectives.filter('cona_range', function(){
    return function(conas, rango){ 
	if (!rango){return conas};
	selected = [];
	conas.forEach(function(cona){
	    if (cona.precio >= rango.minimo && cona.precio <= rango.maximo){ selected.push(cona);  }
	});
	return selected;
    };
});

fhDirectives.filter('range', function(){
    return function(rango){
	return rango.minimo + " - " + rango.maximo;
    };
});

fhDirectives.filter('filtrar', function(){
    
    return function(coleccion, criterio){
	resultado = [];
	coleccion.forEach(function(elemento){ 
	    if (criterio.elemento) { 
		resultado.push(elemento);
	    } 
	});
	return resultado;
    };
    return filtrar;

});
