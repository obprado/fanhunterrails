class CreateArquetipo < ActiveRecord::Migration
  def up
    remove_column :narizones, :arquetipo

    create_table :arquetipos do |t|
      t.string :nombre
      t.integer :puntos_de_atributo
      t.integer :puntos_de_habilidad
      t.integer :puntos_libres
      t.timestamp
    end

    change_table :narizones do |t|
      t.belongs_to :arquetipo
    end
    
    change_table :conas do |t|
      t.belongs_to :arquetipo
    end

    change_table :taras do |t|
      t.belongs_to :arquetipo
    end

    change_table :habilidades do |t|
      t.belongs_to :arquetipo
    end

    change_table :habilidades_opcionales do |t|
      t.belongs_to :arquetipo
    end

  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
