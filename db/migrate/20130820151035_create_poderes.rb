class CreatePoderes < ActiveRecord::Migration
  def change
    create_table :poderes do |table|
      table.string 'nombre'
      table.integer 'nivel'
      table.integer 'coste'
      table.belongs_to :atributo
      table.timestamp
    end
  end
end
