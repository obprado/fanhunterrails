class ChangePrecioFromPoder < ActiveRecord::Migration
  def change
    rename_column :poderes, :precio, :coste
  end
end
