class Api::ConasController < ApplicationController

  def show
    respond_with Cona.find params[:id]
  end

  def create
    @cona = Cona.new 
    @cona.save
    respond_with :api, @cona
  end


  def destroy
    Cona.find(params[:id]).destroy
    render nothing: true
  end

end
