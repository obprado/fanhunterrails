class Tecnico < ArquetipoReal

  def aplicar_modificadores
    Habilidad.class_eval do
      alias gasto_antiguo gasto

      def gasto
        @atributos_de_tecnico = ["neuronas", "agallas", "empatia", "reflejos"]
        if @atributos_de_tecnico.include? habilidad_de_catalogo.atributo_de_catalogo.nombre
          puntos = []
          cantidad_puntos_gratis = atributo.saldos.select { |saldo| saldo.tipo_de_punto.eql? nombre.to_sym }.size
          puntos_de_gasto = ((nivel - cantidad_puntos_gratis)/ 2.0)
          puntos_de_gasto = puntos_de_gasto.round + cantidad_puntos_gratis
          puntos_de_gasto.times do
            puntos.push PuntoDeHabilidadEspecifica.new :habilidad => nombre.to_sym
          end
          Gasto.new :puntos => puntos
        else
          gasto_antiguo
        end        
      end

    end
  end

end
