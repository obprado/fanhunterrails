class DeleteAttributesFromCharacters < ActiveRecord::Migration
  def up
    drop_table 'characters'
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
