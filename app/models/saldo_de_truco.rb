class SaldoDeTruco < Saldo
  
  def puede_pagar? punto
    super or (punto.tipo_de_punto.eql? :punto_libre_de_poder and disponible?)
  end

  def descomponer! coleccion_de_puntos
    coleccion_de_puntos.delete self
  end

end
