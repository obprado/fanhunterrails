class AtributoDeCatalogo < ActiveRecord::Base
  has_many :catalogo_de_poderes
  has_many :atributos

  belongs_to :habilidad_gratuita, :class_name => 'HabilidadDeCatalogo', :foreign_key => 'habilidad_gratuita_de_catalogo_id'

#  has_one :habilidad_gratuita, :class_name => 'HabilidadDeCatalogo', :foreign_key => 'habilidad_gratuita_de_catalogo_id'

  belongs_to :tipo_de_atributo

  has_many :catalogo_de_habilidades
  has_one :habilidad_opcional_de_catalogo

  def tipo
    tipo_de_atributo.tipo
  end

end
