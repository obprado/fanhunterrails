class PuntoLibreDePoderPorAtributo < PuntoEspecifico

  attr_accessor :poder_de_catalogo

  def initialize attributes = {}
    attributes.each do |nombre, valor|
      send("#{nombre}=", valor)
    end
    self.especificidad = poder_de_catalogo.atributo_de_catalogo.nombre.to_sym
  end

  def nombre
    poder_de_catalogo.nombre
  end
  
  def descomponerse
    hijos = []
    hijos.push PuntoLibreDePoder.new 
    hijos
  end
  
end
