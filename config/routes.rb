FanHunter::Application.routes.draw do
  namespace :api do
    resources :narizones, :poderes, :taras, :conas
    resources :catalogo_de_poderes, :catalogo_de_conas, :catalogo_de_taras, :arquetipos, :only => [:index, :show]
    post '/narizones/validar', :to => 'narizones#validate'
  end
  root :to => redirect('/public/index.html')
  get '/narizones', :to => redirect('/')
end
