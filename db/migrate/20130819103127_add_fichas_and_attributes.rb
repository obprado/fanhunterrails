class AddFichasAndAttributes < ActiveRecord::Migration
  def change
    create_table :fichas do |table|
      table.string :nombre
      table.string :arquetipo
      table.timestamp
    end
    create_table :atributos do |t|
      t.belongs_to :ficha
      t.string :nombre
      t.integer :nivel
      t.timestamp
    end
    create_table :habilidades do |t|
      t.belongs_to :atributo
      t.string :nombre
      t.integer :nivel
    end
  end
end
