var entities = angular.module('fhEntities', ['ngResource','config']);

entities.value('operacionesRest', ['query','get','save','update','remove', 'create']);
entities.value('atributoFuerte', 4);

entities.factory('clienteRest', function($resource, HOST, $http){
    $http.defaults.headers.common.Accept = "application/json";
    return function(tipoDato){
	return $resource(HOST + '/api/' + tipoDato + '/:id', 
			 {id: "@id"},
			 {
			     "update": {method: "PUT"},
			     "create": {method: "POST"}
			     }
			);
    };
});

entities.factory('copyElements', function(){
    return function(elementNames, source, destiny){
	elementNames.forEach(function(elemento){
	    destiny[elemento] = source[elemento];
	});
    };
});

entities.factory('Coleccion', function(){
    
    var Coleccion = function(array, prototipo){
	resultado = [];
	for (var elemento in array){
	    if (array[elemento].id){
		resultado.push(new prototipo(array[elemento]));
	    }
	}
	return resultado;

    };

    return Coleccion;

});

entities.factory('Categoria', function(){
    
    var Categoria = function(object){
	this.minimo = object.minimo;
	this.maximo = object.maximo;
    };

    Categoria.prototype.test = function(){alert(47);};

    return Categoria;

});

entities.factory('ConaDeCatalogo', function(copyElements, operacionesRest, clienteRest, Coleccion){
    
    var restClient = clienteRest('catalogo_de_conas');

    var ConaDeCatalogo = function(json){
	if (json){
	    this.id = json.id;
	    this.precio = json.precio;
	    this.nombre = json.nombre;
	}
    };

    copyElements(operacionesRest, restClient, ConaDeCatalogo.prototype);

    ConaDeCatalogo.prototype.query = function(callback){
	restClient.query(function(JSON){
	    callback(Coleccion(JSON, ConaDeCatalogo));
	});
    };

    ConaDeCatalogo.prototype.categorias = function(){
	return [{id: 1, minimo:1, maximo: 3},{id: 1, minimo:4, maximo: 6},{id: 1, minimo:7, maximo: 9},{id: 1, minimo:10, maximo: 12}];
    };

    copyElements(operacionesComunes, ConaDeCatalogo.prototype, ConaDeCatalogo);

    return ConaDeCatalogo;

});

entities.factory('Cona', function(copyElements, operacionesRest, clienteRest, Coleccion, ConaDeCatalogo){

    var restClient = clienteRest('conas');

    var Cona = function(JSON_cona){
        this.id = JSON_cona.id;
        this.precio = JSON_cona.precio;
        this.cona_de_catalogo = new ConaDeCatalogo(JSON_cona.cona_de_catalogo);
    };

    copyElements(operacionesRest, restClient, Cona.prototype);

    Cona.prototype.create = function(callback){
        restClient.create(JSON.stringify(this), callback);
    };

    Cona.prototype.get = function(id){
	return new Cona(restClient.get({id: id}));
    };
    
    copyElements(operacionesRest, Cona.prototype, Cona);

    return Cona;

});

entities.factory('TaraDeCatalogo', function(copyElements, operacionesRest, clienteRest, Coleccion){

    operacionesComunes = operacionesRest.concat(['categorias']);
    
    var restClient = clienteRest('catalogo_de_taras');

    var TaraDeCatalogo = function(json){
	if (json){
	    this.id = json.id;
	    this.valor = json.valor;
	    this.nombre = json.nombre;
	}
    };

    copyElements(operacionesRest, restClient, TaraDeCatalogo.prototype);

    TaraDeCatalogo.prototype.query = function(callback){
	restClient.query(function(JSON){
	    callback(Coleccion(JSON, TaraDeCatalogo));
	});
    };

    TaraDeCatalogo.prototype.categorias = function(){
	return [{id: 1, minimo:1, maximo: 3},{id: 1, minimo:4, maximo: 6},{id: 1, minimo:7, maximo: 9},{id: 1, minimo:10, maximo: 12}];
    };

    TaraDeCatalogo.prototype.pertenece = function(){
	self = this;
	return function(categoria){
	    return (  (self.valor > categoria.minimo) && ( self.valor < categoria.maximo)  );
	}
    };

    copyElements(operacionesComunes, TaraDeCatalogo.prototype, TaraDeCatalogo);
    return TaraDeCatalogo;

});

entities.factory('Tara', function(copyElements, operacionesRest, clienteRest, Coleccion, TaraDeCatalogo){

    var restClient = clienteRest('taras');

    var Tara = function(JSON_tara){
        this.id = JSON_tara.id;
        this.valor = JSON_tara.valor;
        this.tara_de_catalogo = new TaraDeCatalogo(JSON_tara.tara_de_catalogo);
    };

    copyElements(operacionesRest, restClient, Tara.prototype);

    Tara.prototype.create = function(callback){
        restClient.create(JSON.stringify(this), callback);
    };

    Tara.prototype.get = function(id){
	return new Tara(restClient.get({id: id}));
    };
    
    copyElements(operacionesRest, Tara.prototype, Tara);

    return Tara;

});

entities.factory('PoderDeCatalogo', function(copyElements, operacionesRest, clienteRest, Coleccion){
    
    operacionesComunes = operacionesRest.concat(['getNivel']);

    var restClient = clienteRest('catalogo_de_poderes');

    var PoderDeCatalogo = function(json){
	if (json){
	    this.id = json.id;
	    this.nombre = json.nombre;
	    this.precio = json.precio;
	    this.atributo_de_catalogo_id = json.atributo_de_catalogo_id;
	};
    };

    copyElements(operacionesRest, restClient, PoderDeCatalogo.prototype);

    PoderDeCatalogo.prototype.getNivel = function(narizon){	
	atributo = narizon.getAtributo(this.atributo_de_catalogo_id);
	return atributo.nivel;
    };

    PoderDeCatalogo.prototype.query = function(callback){
	restClient.query(function(JSON){
	    coleccion = Coleccion(JSON, PoderDeCatalogo);
	    callback(coleccion);
	});
    };

    copyElements(operacionesComunes, PoderDeCatalogo.prototype, PoderDeCatalogo);

    return PoderDeCatalogo;

});

entities.factory('Poder', function(copyElements, operacionesRest, clienteRest, Coleccion, PoderDeCatalogo){

    var restClient = clienteRest('poderes');

    var Poder = function(JSON_poder){
        this.id = JSON_poder.id;
        this.nivel = JSON_poder.nivel;
        this.poder_de_catalogo = new PoderDeCatalogo(JSON_poder.poder_de_catalogo);

	Object.defineProperty(this, "precio", { 
	    get : function(){ return this.poder_de_catalogo.precio; }  
	});	    
    };


    copyElements(operacionesRest, restClient, Poder.prototype);

    Poder.prototype.create = function(callback){
        restClient.create(JSON.stringify(this), callback);
    };

    Poder.prototype.get = function(id){
	return new Poder(restClient.get({id: id}));
    };
    
    copyElements(operacionesRest, Poder.prototype, Poder);

    return Poder;

});

entities.factory('Atributo', function(copyElements, operacionesRest, clienteRest, Coleccion, atributoFuerte){

    operacionesComunes = operacionesRest.concat(['esDebil', 'esFuerte']);

    var restClient = clienteRest('atributos');

    var Atributo = function(json){
	this.id = json.id;
	this.nivel = json.nivel;
	this.atributos = json.atributos;
	this.habilidades = json.habilidades;
	this.habilidades_opcionales = json.habilidades_opcionales;
	this.atributo_de_catalogo = json.atributo_de_catalogo;

	Object.defineProperty(this, "nombre", { 
	    get : function(){ return this.atributo_de_catalogo.nombre; }  
	});	    
    };
    

    copyElements(operacionesComunes, restClient, Atributo.prototype);

    Atributo.prototype.add_habilidad_opcional = function(){
	this.habilidades_opcionales.push({});
    };

    Atributo.prototype.delete_habilidad_opcional = function(habilidad_opcional){
	this.habilidades_opcionales.push();
    };

    Atributo.prototype.esSuyo = function(){
	self = this;
	return function(poder){
	    return poder.atributo_de_catalogo_id == self.atributo_de_catalogo.id;
	};
    };

    Atributo.prototype.esFuerte = function(){
	return this.nivel >= atributoFuerte;
    };

    Atributo.prototype.esDebil = function(){
	return this.nivel < atributoFuerte;
    };

    copyElements(operacionesComunes, Atributo.prototype, Atributo);

    return Atributo;

});

entities.factory('Arquetipo', function(copyElements, operacionesRest, clienteRest){

    var restClient = clienteRest('arquetipos');

    var Arquetipo = function(JSON_arquetipo){
	this.id = JSON_arquetipo.id;
	this.nombre = JSON_arquetipo.nombre;
	this.puntos_de_atributo = JSON_arquetipo.puntos_de_atributo;
	this.puntos_de_habilidad = JSON_arquetipo.puntos_de_habilidad;
	this.puntos_libres = JSON_arquetipo.puntos_libres;
    };

    copyElements(operacionesRest, restClient, Arquetipo.prototype);

    Arquetipo.prototype.find_by_narizon = function(arquetipos, narizon){
	arquetipos.forEach(function(arquetipo){
	    if (arquetipo.id == narizon.arquetipo.id){
		return arquetipo;
	    }
	});
    };

    copyElements(operacionesRest, Arquetipo.prototype, Arquetipo);

    return Arquetipo;
});

entities.factory('Narizon', function(Arquetipo, Poder, Atributo, copyElements, operacionesRest, clienteRest, Coleccion, PoderDeCatalogo, TaraDeCatalogo, Tara, Cona, ConaDeCatalogo, HOST, $http){

    operacionesComunes = operacionesRest.concat(['validar']);

    var restClient = clienteRest('narizones');

    var Narizon = function(JSON_narizon){
	if (JSON_narizon){
	    this.id = JSON_narizon.id;
	    this.nombre = JSON_narizon.nombre;
	    this.jugador = JSON_narizon.jugador;
	    this.arquetipo = new Arquetipo(JSON_narizon.arquetipo);
	    this.atributos = Coleccion(JSON_narizon.atributos, Atributo);
	    this.poderes = Coleccion(JSON_narizon.poderes, Poder);
	    this.conas = Coleccion(JSON_narizon.conas, Cona);
	    this.taras = Coleccion(JSON_narizon.taras, Tara);
	    
	    this.atributos.sort(function(a,b){return a.atributo_de_catalogo.orden - b.atributo_de_catalogo.orden});
	}
    };

    copyElements(operacionesComunes, restClient, Narizon.prototype);

    Narizon.prototype.get = function(id, callback){
	restClient.get({id: id}, function(JSON){
	    callback(new Narizon(JSON)); 
	});
    };

    Narizon.prototype.getAtributos = function(filtro){ 
	result = [];
	this.atributos.forEach(function(atributo){
	    if (filtro.apply(atributo)) { result.push(atributo) }
	});
	return result;
    };

    Narizon.prototype.addPoder = function(poder){ 
	if (poder instanceof PoderDeCatalogo){
	    narizon = this;
	    Poder.create(function(json_poder){
		newPoder = new Poder(json_poder);		
		newPoder.poder_de_catalogo = poder;
		atributo = narizon.getAtributo(poder.atributo_de_catalogo_id);
		newPoder.nivel = atributo.nivel;
		narizon.poderes.push(newPoder);
	    });
	}
    };

    Narizon.prototype.addTara = function(tara){ 
	if (tara instanceof TaraDeCatalogo){
	    narizon = this;
	    Tara.create(function(json_tara){
		newTara = new Tara(json_tara);		
		newTara.tara_de_catalogo = tara;
		narizon.taras.push(newTara);
	    });
	}
    };

    Narizon.prototype.addCona = function(cona){ 
	if (cona instanceof ConaDeCatalogo){
	    narizon = this;
	    Cona.create(function(json_cona){
		newCona = new Cona(json_cona);		
		newCona.cona_de_catalogo = cona;
		narizon.conas.push(newCona);
	    });
	}
    };

    Narizon.prototype.getAtributo = function(atributo_de_catalogo_id){
	for (var index in this.atributos){
	    if (this.atributos[index].atributo_de_catalogo.id == atributo_de_catalogo_id){
		result = this.atributos[index];
	    }
	}
	return result;
    };

    Narizon.prototype.validar = function(saldosCallback, facturaCallback){
	
	$http.post(HOST + '/api/narizones/validar', this).success(function(response){ 
	    
	    monedero = [];
	    response.saldos.forEach(function(elemento){
	
		if (!monedero[elemento.tipo_de_punto]){
		    monedero[elemento.tipo_de_punto] = 0;
//		    alert(monedero[elemento.tipo_de_punto].toSource());
		}
    
		if (elemento.disponible){
		    monedero[elemento.tipo_de_punto]++;
		}               

	    });
  //          alert(monedero.punto_de_atributo);

	    impagados = [];
//	    alert(response.factura.toSource());
	    response.factura.forEach(function(elemento){
	
		if (!impagados[elemento.tipo_de_punto]){
		    impagados[elemento.tipo_de_punto] = 0;
		}
		    impagados[elemento.tipo_de_punto]++;    
/*		if (elemento.disponible){

		}               */                
	    });

	    saldosCallback(resultado);
	    facturaCallback(impagados);

	});
    };

    copyElements(operacionesComunes, Narizon.prototype, Narizon);

    return Narizon;
});